import defaultTheme from 'tailwindcss/defaultTheme';
import forms from '@tailwindcss/forms';
import typography from '@tailwindcss/typography';
import preset from './vendor/filament/support/tailwind.config.preset'

/** @type {import('tailwindcss').Config} */
export default {
    content: [
        './vendor/laravel/framework/src/Illuminate/Pagination/resources/views/*.blade.php',
        './vendor/laravel/jetstream/**/*.blade.php',
        './storage/framework/views/*.php',
        './resources/views/**/*.blade.php',
        './app/Filament/**/*.php',
        './resources/views/filament/**/*.blade.php',
        './vendor/filament/**/*.blade.php',
        './resources/mail/**/*.mjml'
    ],
    darkMode: 'class',
    theme: {
        container: {
            center: true,
            padding: '1rem'
        },
        extend: {
            fontFamily: {
                sans: ['Roboto', ...defaultTheme.fontFamily.sans],              
                futura: ['Kumbh Sans', ...defaultTheme.fontFamily.sans],              
            },
            colors: {
                primary: {
                    50: "#FBE9EC",
                    100: "#F7CFD5",
                    200: "#EEA0AA",
                    300: "#E77483",
                    400: "#DE4459",
                    500: "#C82338",
                    600: "#A11C2E",
                    700: "#7A1523",
                    800: "#4E0E16",
                    900: "#27070B",
                    950: "#160406"
                  },
                secondary: {
                    50: "#D7EEF9",
                    100: "#B3DFF5",
                    200: "#66BEEA",
                    300: "#1E9DDC",
                    400: "#146690",
                    500: "#092F42",
                    600: "#072636",
                    700: "#061D28",
                    800: "#04131B",
                    900: "#020A0D",
                    950: "#010304"
                  }
            },
            presets: [preset],
        },
    },

    plugins: [forms, typography],
};
