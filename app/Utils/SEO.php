<?php

namespace App\Utils;

use Artesaos\SEOTools\Facades\OpenGraph;
use Artesaos\SEOTools\Facades\SEOMeta;


class SEO
{

    public function __construct(string $title, string $description, string $keyWords, string $url)
    {
        SEOMeta::setTitle($title, false);
        SEOMeta::setDescription($description);
        SEOMeta::setKeywords(__($keyWords));
        SEOMeta::setCanonical($url);

        OpenGraph::setTitle(__($title));
        OpenGraph::setDescription($description);
        OpenGraph::setUrl($url);
        OpenGraph::setType('website');

        SEOMeta::addMeta('geo.placename', config('site.seo.geo_placename'));
        SEOMeta::addMeta('geo.region', config('site.seo.geo_region'));
        SEOMeta::addMeta('geo.position', config('site.seo.geo_position'));
        foreach (config('site.seo.property') as $key => $value) {
            OpenGraph::addProperty($key, $value);
        }
        OpenGraph::addProperty('language', app()->getLocale());
    }

    public static function generate(string $title = null, string $description = null)
    {
        $title = $title ?? __(config('site.seo.title'));
        $description = $description ?? __(config('site.seo.description'));
        new static($title, $description, config('site.seo.keywords'), env('APP_URL'));

    }


}