<?php

namespace App\Providers;

// use Illuminate\Support\Facades\Gate;
use Filament\Facades\Filament;
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The model to policy mappings for the application.
     *
     * @var array<class-string, class-string>
     */
    protected $policies = [
        //
    ];

    /**
     * Register any authentication / authorization services.
     */
    public function boot(): void
    {
        Gate::before(function ($user, $ability) {
            return Filament::auth()->user()->hasRole('super-admin') ? true : null;
        });
    }
}
