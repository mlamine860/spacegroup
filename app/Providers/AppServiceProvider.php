<?php

namespace App\Providers;

use App\Utils\SEO;
use App\Models\Testimony;
use Illuminate\Support\ServiceProvider;
use Stephenjude\FilamentBlog\Models\Post;
use Filament\Support\Facades\FilamentColor;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
        //
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        FilamentColor::register([
            'primary' => [
                50 => "#FBE9EC",
                100 => "#F7CFD5",
                200 => "#EEA0AA",
                300 => "#E77483",
                400 => "#DE4459",
                500 => "#C82338",
                600 => "#A11C2E",
                700 => "#7A1523",
                800 => "#4E0E16",
                900 => "#27070B",
                950 => "#160406"
            ],
            'secondary' => [
                50 => "#D7EEF9",
                100 => "#B3DFF5",
                200 => "#66BEEA",
                300 => "#1E9DDC",
                400 => "#146690",
                500 => "#092F42",
                600 => "#072636",
                700 => "#061D28",
                800 => "#04131B",
                900 => "#020A0D",
                950 => "#010304"
            ]
        ]);
        if (!app()->runningInConsole()) {
            view()->share([
                'testimonies' => Testimony::latest()->limit(5)->get(),
                'posts' => Post::published()->latest()->with('tags')->limit(3)->get(),
            ]);
            SEO::generate();
        }

    }
}
