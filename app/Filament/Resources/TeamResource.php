<?php

namespace App\Filament\Resources;

use App\Filament\Resources\TeamResource\Pages;
use App\Models\Team;
use Filament\Forms;
use Filament\Forms\Form;
use Filament\Resources\Resource;
use Filament\Tables;
use Filament\Tables\Table;

class TeamResource extends Resource
{
    protected static ?string $model = Team::class;
    protected static ?string $modelLabel = 'Equipe';

    protected static ?string $navigationIcon = 'heroicon-o-user-group';

    public static function form(Form $form): Form
    {
        return $form
            ->schema([
                Forms\Components\TextInput::make('first_name')
                    ->required()
                    ->maxLength(50)
                    ->label('Prénom'),
                Forms\Components\TextInput::make('last_name')
                    ->required()
                    ->maxLength(50)
                    ->label('Nom'),
                Forms\Components\TextInput::make('email')
                    ->email()
                    ->required()
                    ->maxLength(120)
                    ->unique(ignoreRecord: true),
                Forms\Components\TextInput::make('role')
                    ->required()
                    ->maxLength(255),
                Forms\Components\FileUpload::make('photo')
                    ->required()
                    ->directory('teams')
                    ->image()->columnSpanFull(),
                Forms\Components\Repeater::make('handles')
                    ->label('Reseaux sociaux')
                    ->schema([
                        Forms\Components\Select::make('Social')
                            ->options([
                                'facebook' => 'Facebook',
                                'linked-in' => 'FLinked In',
                                'instagram' => 'Instagram',
                                'twitter-x' => 'Twitter-X',
                            ]),
                        Forms\Components\TextInput::make('link')
                            ->required()
                            ->url()
                            ->prefix('https://')
                            ->label('Liens'),
                    ])
                    ->columnSpanFull()
                    ->addActionLabel('Ajouter un reseaux social'),
                Forms\Components\Textarea::make('bio')
                    ->columnSpanFull(),
            ]);
    }

    public static function table(Table $table): Table
    {
        return $table
            ->columns([
                Tables\Columns\ImageColumn::make('photo')
                    ->circular()
                    ->searchable(),
                Tables\Columns\TextColumn::make('fullName')
                    ->searchable()
                    ->label('Nom'),
                Tables\Columns\TextColumn::make('email')
                    ->searchable(),
                Tables\Columns\TextColumn::make('role')
                    ->searchable(),
            ])
            ->filters([
                //
            ])
            ->actions([
                Tables\Actions\EditAction::make(),
            ])
            ->bulkActions([
                Tables\Actions\BulkActionGroup::make([
                    Tables\Actions\DeleteBulkAction::make(),
                ]),
            ]);
    }

    public static function getRelations(): array
    {
        return [
            //
        ];
    }

    public static function getPages(): array
    {
        return [
            'index' => Pages\ListTeams::route('/'),
            'create' => Pages\CreateTeam::route('/create'),
            'edit' => Pages\EditTeam::route('/{record}/edit'),
        ];
    }
}
