<?php

namespace App\Filament\Resources;

use Filament\Forms;
use Filament\Tables;
use Filament\Forms\Form;
use Filament\Tables\Table;
use App\Models\Application;
use Filament\Infolists\Infolist;
use Filament\Resources\Resource;
use Filament\Tables\Actions\ViewAction;
use Illuminate\Support\Facades\Storage;
use Filament\Infolists\Components\Actions;
use Filament\Infolists\Components\Section;
use Filament\Infolists\Components\TextEntry;
use Filament\Infolists\Components\Actions\Action;
use App\Filament\Resources\ApplicationResource\Pages;
use Zip;

class ApplicationResource extends Resource
{
    protected static ?string $model = Application::class;

    protected static ?string $navigationIcon = 'heroicon-o-folder';

    protected static ?string $modelLabel = 'Candidature';

    public static function form(Form $form): Form
    {
        return $form
            ->schema([
                Forms\Components\TextInput::make('first_name')
                    ->required()
                    ->maxLength(50),
                Forms\Components\TextInput::make('last_name')
                    ->required()
                    ->maxLength(50),
                Forms\Components\TextInput::make('email')
                    ->email()
                    ->required()
                    ->maxLength(120),
                Forms\Components\TextInput::make('phone_number')
                    ->tel()
                    ->required()
                    ->maxLength(20),
                Forms\Components\TextInput::make('address')
                    ->required()
                    ->maxLength(255),
                Forms\Components\TextInput::make('city')
                    ->required()
                    ->maxLength(255),
                Forms\Components\Textarea::make('cover_letter')
                    ->required()
                    ->columnSpanFull(),
                Forms\Components\TextInput::make('cv')
                    ->required()
                    ->maxLength(255),
                Forms\Components\TextInput::make('additional_docs')
                    ->required(),
            ]);
    }

    public static function table(Table $table): Table
    {
        return $table
            ->columns([
                Tables\Columns\TextColumn::make('fullName')
                    ->searchable()
                    ->label('Nom complet'),
                Tables\Columns\TextColumn::make('email')
                    ->searchable()
                    ->label('E-mail adresse'),
                Tables\Columns\TextColumn::make('phone_number')
                    ->searchable()
                    ->label('Numéro de téléphone'),
                Tables\Columns\TextColumn::make('created_at')
                    ->dateTime()
                    ->sortable()
                    ->toggleable(isToggledHiddenByDefault: true),
                Tables\Columns\TextColumn::make('updated_at')
                    ->dateTime()
                    ->sortable()
                    ->toggleable(isToggledHiddenByDefault: true),
            ])
            ->filters([
                //
            ])
            ->actions([
                ViewAction::make()
            ])
            ->bulkActions([
                Tables\Actions\BulkActionGroup::make([
                    Tables\Actions\DeleteBulkAction::make(),
                ]),
            ]);
    }

    public static function infoList(Infolist $infolist): InfoList
    {
        return $infolist->schema([
            Section::make('Information personnel')
                ->schema([
                    TextEntry::make('fullName')
                        ->label('Nom complet'),
                    TextEntry::make('email')
                        ->label('Adresse E-mail'),
                    TextEntry::make('phone_number')
                        ->label('Numéro de téléphone'),
                    TextEntry::make('address')
                        ->label('Adresse'),
                    TextEntry::make('city')
                        ->label('Ville')
                        ->columnSpanFull(),
                    TextEntry::make('cover_letter')
                        ->label('Lettre de motivation')
                        ->columnSpanFull(),


                ])->columns(2),
            Section::make('Documents à télécharger')
                ->schema([
                    Actions::make([
                        Action::make('cv')
                            ->icon('heroicon-m-arrow-down-on-square')
                            ->label('Cv')
                            ->action(function (Application $application) {
                                return Storage::download($application->cv, "{$application->fullName} Cv");
                            }),
                        Action::make('doc')
                            ->icon('heroicon-m-arrow-down-on-square')
                            ->label('Documents additionelles')
                            ->action(function (Application $application) {
                                $zip = Zip::create("{$application->fullName} Additional Docs.zip");
                                collect($application->additional_docs)->each(fn($doc) => $zip->add(Storage::path($doc)));
                                return $zip;
                            }),

                    ]),
                ])
        ]);

    }

    public static function getRelations(): array
    {
        return [
            //
        ];
    }

    public static function getPages(): array
    {
        return [
            'index' => Pages\ListApplications::route('/'),
            'view' => Pages\ViewApplication::route('/{record}')
        ];
    }
}
