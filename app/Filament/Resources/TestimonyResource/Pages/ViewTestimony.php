<?php

namespace App\Filament\Resources\TestimonyResource\Pages;

use Filament\Actions\Action;
use Filament\Resources\Pages\ViewRecord;
use App\Filament\Resources\TestimonyResource;
use App\Filament\Resources\TestimonyResource\Pages\EditTestimony;

class ViewTestimony extends ViewRecord
{
    protected static string $resource = TestimonyResource::class;



    protected function getHeaderActions():array
    {
        return [
            Action::make(__('Edit'))
            ->url(EditTestimony::getUrl(['record' => $this->getRecord()]))
        ];
    }
}
