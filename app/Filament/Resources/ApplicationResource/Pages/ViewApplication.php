<?php

namespace App\Filament\Resources\ApplicationResource\Pages;

use App\Filament\Resources\ApplicationResource;
use Filament\Resources\Pages\ViewRecord;
use Filament\Actions;

class ViewApplication extends ViewRecord
{
    protected static string $resource = ApplicationResource::class;
    protected ?string $heading  = 'Details du candidature';


    protected function getHeaderActions(): array
    {
        return [
            Actions\DeleteAction::make(),
        ];
    }
}
