<?php

namespace App\Filament\Resources;

use Filament\Tables;
use Filament\Forms\Form;
use App\Models\Testimony;
use Filament\Tables\Table;
use Filament\Infolists\Infolist;
use Filament\Resources\Resource;
use Filament\Forms\Components\Select;
use Filament\Forms\Components\Textarea;
use Filament\Tables\Columns\TextColumn;
use Filament\Forms\Components\TextInput;
use Filament\Tables\Columns\ImageColumn;
use Filament\Forms\Components\FileUpload;
use Filament\Infolists\Components\TextEntry;
use Filament\Infolists\Components\ImageEntry;
use App\Filament\Resources\TestimonyResource\Pages;

class TestimonyResource extends Resource
{
    protected static ?string $model = Testimony::class;

    protected static ?string $navigationIcon = 'quote';



    public static function form(Form $form): Form
    {
        return $form
            ->schema([
                TextInput::make('name')
                    ->label(__('Nom complet'))
                    ->required()
                    ->minLength(5)
                    ->maxLength(60)
                    ->unique(ignoreRecord: true),
                TextInput::make('job')
                    ->label(__('Work'))
                    ->required()
                    ->minLength(5)
                    ->maxLength(255),
                FileUpload::make('avatar')
                    ->label(__('Avatar'))
                    ->image()
                    ->directory('testimonies')
                    ->columnSpanFull(),
                Select::make('stars')
                    ->label(__('Stars'))
                    ->required()
                    ->options([
                        '1' => '1 Etoile',
                        '2' => '2 Etoiles',
                        '3' => '3 Etoiles',
                        '4' => '4 Etoiles',
                        '5' => '5 Etoiles',
                    ]),
                Textarea::make('text')
                    ->label(__('Testimony'))
                    ->minLength(10)
                    ->maxLength(500)
                    ->columnSpanFull()
            ]);
    }

    public static function table(Table $table): Table
    {
        return $table
            ->columns([
                ImageColumn::make('avatar')
                    ->circular()
                    ->toggleable(),
                TextColumn::make('name')
                    ->label(__('Nom'))
                    ->searchable()
                    ->sortable(),
                TextColumn::make('job')
                    ->label(__('Work')),
                TextColumn::make('stars')
                    ->label(__('Stars'))
                    ->sortable()
            ])
            ->filters([
                //
            ])
            ->actions([
                // Tables\Actions\EditAction::make(),
                Tables\Actions\ViewAction::make()
            ])
            ->bulkActions([
                Tables\Actions\BulkActionGroup::make([
                    Tables\Actions\DeleteBulkAction::make(),
                ]),
            ]);
    }

    public static function infolist(Infolist $infolist): Infolist
    {
        return $infolist
            ->schema([
                ImageEntry::make('avatar')
                    ->columnSpan(2),
                TextEntry::make('name')
                    ->label(__('name')),
                TextEntry::make('job')
                    ->label(__('Work')),
                TextEntry::make('text')
                    ->label(__('Testimony'))
                    ->columnSpanFull(),
                TextEntry::make('stars')
                    ->view('stars', ['stars' => $infolist->getRecord()->stars])
            ])->columns(4);
    }

    public static function getRelations(): array
    {
        return [
            //
        ];
    }

    public static function getPages(): array
    {
        return [
            'index' => Pages\ListTestimonies::route('/'),
            'create' => Pages\CreateTestimony::route('/create'),
            'edit' => Pages\EditTestimony::route('/{record}/edit'),
            'view' => Pages\ViewTestimony::route('/{record}')
        ];
    }


    public static function getModelLabel(): string
    {
        return __('Testimonials');
    }
}
