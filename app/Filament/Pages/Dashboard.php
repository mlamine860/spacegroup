<?php

namespace App\Filament\Pages;

use Filament\Facades\Filament;
use App\Filament\Widgets\StatsOverview;
use Chiiya\FilamentAccessControl\Traits\AuthorizesPageAccess;




class Dashboard extends \Filament\Pages\Dashboard
{
    use AuthorizesPageAccess;

    protected static string $view = 'filament.pages.dashboard';

    protected static ?string $navigationIcon = 'dashboard';

    public static string $permission = 'view-dashboard';



    protected function getHeaderWidgets(): array
    {
        return [
            StatsOverview::class
        ];
    }

    public function mount(): void
    {
        static::authorizePageAccess();
    }






}
