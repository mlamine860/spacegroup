<?php

namespace App\Filament\Pages;

use Filament\Pages\Page;
use BezhanSalleh\FilamentGoogleAnalytics\Widgets;
use Chiiya\FilamentAccessControl\Traits\AuthorizesPageAccess;

class GoogleAnalytics extends Page
{
    use AuthorizesPageAccess;
    protected static ?string $navigationIcon = 'heroicon-m-chart-bar';

    protected static string $view = 'filament.pages.google-analytics';

    public static string $permission = 'view-analytics';


    protected function getHeaderWidgets(): array
    {
        return [
            Widgets\PageViewsWidget::class,
            Widgets\VisitorsWidget::class,
            Widgets\ActiveUsersOneDayWidget::class,
            Widgets\ActiveUsersSevenDayWidget::class,
            Widgets\ActiveUsersTwentyEightDayWidget::class,
            Widgets\SessionsWidget::class,
            Widgets\SessionsDurationWidget::class,
            Widgets\SessionsByCountryWidget::class,
            Widgets\SessionsByDeviceWidget::class,
            Widgets\MostVisitedPagesWidget::class,
            Widgets\TopReferrersListWidget::class,
        ];
    }

    public function mount(): void
    {
        static::authorizePageAccess();
    }
}
