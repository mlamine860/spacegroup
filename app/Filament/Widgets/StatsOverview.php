<?php

namespace App\Filament\Widgets;

use App\Models\Team;
use App\Models\Message;
use App\Models\Project;
use App\Models\Testimony;
use Flowframe\Trend\Trend;
use App\Models\Application;
use Flowframe\Trend\TrendValue;
use App\Filament\Resources\TeamResource;
use Stephenjude\FilamentBlog\Models\Post;
use App\Filament\Resources\MessageResource;
use App\Filament\Resources\ProjectResource;
use App\Filament\Resources\TestimonyResource;
use Filament\Widgets\StatsOverviewWidget\Stat;
use App\Filament\Resources\ApplicationResource;
use Stephenjude\FilamentBlog\Resources\PostResource;
use Filament\Widgets\StatsOverviewWidget as BaseWidget;

class StatsOverview extends BaseWidget
{
    protected function getStats(): array
    {
        return [
            Stat::make('Projets', Project::count())
                ->descriptionIcon('diagram-projects')
                ->description('Projets actifs')
                ->chart($this->getTrendValue(Project::class))
                ->color('success')
                ->url(ProjectResource::getUrl('index')),
            Stat::make('Candidudatures', Application::count())
                ->chart($this->getTrendValue(Application::class))
                ->description('Candidatures postuler')
                ->descriptionIcon('heroicon-o-folder')
                ->url(ApplicationResource::getUrl('index')),
            Stat::make('Témoignages', Testimony::count())
                ->chart($this->getTrendValue(Testimony::class))
                ->descriptionIcon('quote')
                ->description('Témoignages dispo')
                ->url(TestimonyResource::getUrl('index')),
            Stat::make('Membres', Team::count())
                ->chart($this->getTrendValue(Team::class))
                ->descriptionIcon('heroicon-o-user-group')
                ->description('Equipe')
                ->url(TeamResource::getUrl('index')),
            Stat::make('Messages', Message::count())
                ->chart($this->getTrendValue(Message::class))
                ->descriptionIcon('heroicon-o-chat-bubble-left-right')
                ->description('Message')
                ->url(MessageResource::getUrl('index')),
            Stat::make('Blog', Post::count())
                ->chart($this->getTrendValue(Post::class))
                ->descriptionIcon('heroicon-o-document-text')
                ->description('Article')
                ->url(PostResource::getUrl('index')),
        ];
    }

    private function getTrendValue(string $model)
    {
        $data = Trend::model($model)
            ->between(
                start: now()->startOfYear(),
                end: now()->endOfYear(),
            )
            ->perMonth()
            ->count();
        return $data->map(fn(TrendValue $value) => $value->aggregate)->toArray();
    }

}
