<?php

namespace App\Policies;

use App\Models\Application;
use Chiiya\FilamentAccessControl\Models\FilamentUser;

class ApplicationPolicy
{
    /**
     * Determine whether the user can view any models.
     */
    public function viewAny(FilamentUser $user): bool
    {
        return $user->can('view-application');
    }

    /**
     * Determine whether the user can view the model.
     */
    public function view(FilamentUser $user, Application $application): bool
    {
        return $user->can('view-application');
    }

    /**
     * Determine whether the user can create models.
     */
    public function create(FilamentUser $user): bool
    {
        //
    }

    /**
     * Determine whether the user can update the model.
     */
    public function update(FilamentUser $user, Application $application): bool
    {
        //
    }

    /**
     * Determine whether the user can delete the model.
     */
    public function delete(FilamentUser $user, Application $application): bool
    {
        return $user->can('delete-application');
    }
    /**
     * Determine whether the user can delete the model.
     */
    public function deleteAny(FilamentUser $user): bool
    {
        return $user->can('delete-application');
    }

    /**
     * Determine whether the user can restore the model.
     */
    public function restore(FilamentUser $user, Application $application): bool
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the model.
     */
    public function forceDelete(FilamentUser $user, Application $application): bool
    {
        //
    }
}
