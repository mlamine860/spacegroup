<?php

namespace App\Policies;

use App\Models\Message;
use Illuminate\Auth\Access\Response;
use Chiiya\FilamentAccessControl\Models\FilamentUser;

class MessagePolicy
{
    /**
     * Determine whether the user can view any models.
     */
    public function viewAny(FilamentUser $user): bool
    {
        return $user->can('view-message');
    }

    /**
     * Determine whether the user can view the model.
     */
    public function view(FilamentUser $user, Message $message): bool
    {
        return $user->can('view-message');
    }

    /**
     * Determine whether the user can create models.
     */
    public function create(FilamentUser $user): bool
    {
        //
    }

    /**
     * Determine whether the user can update the model.
     */
    public function update(FilamentUser $user, Message $message): bool
    {
        //
    }

    /**
     * Determine whether the user can delete the model.
     */
    public function delete(FilamentUser $user, Message $message): bool
    {
        return $user->can('delete-message');
    }
    /**
     * Determine whether the user can delete the model.
     */
    public function deleteAny(FilamentUser $user): bool
    {
        return $user->can('delete-message');
    }

    /**
     * Determine whether the user can restore the model.
     */
    public function restore(FilamentUser $user, Message $message): bool
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the model.
     */
    public function forceDelete(FilamentUser $user, Message $message): bool
    {
        //
    }
}
