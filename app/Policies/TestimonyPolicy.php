<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Testimony;
use Illuminate\Auth\Access\Response;
use Chiiya\FilamentAccessControl\Models\FilamentUser;

class TestimonyPolicy
{
    /**
     * Determine whether the user can view any models.
     */
    public function viewAny(FilamentUser $user): bool
    {
        return $user->can('view-testimony');
    }

    /**
     * Determine whether the user can view the model.
     */
    public function view(FilamentUser $user, Testimony $testimony): bool
    {
        return $user->can('view-testimony');
    }

    /**
     * Determine whether the user can create models.
     */
    public function create(FilamentUser $user): bool
    {
        return $user->can('create-testimony');
    }

    /**
     * Determine whether the user can update the model.
     */
    public function update(FilamentUser $user, Testimony $testimony): bool
    {
        return $user->can('update-testimony');
    }

    /**
     * Determine whether the user can delete the model.
     */
    public function delete(FilamentUser $user, Testimony $testimony): bool
    {
        return $user->can('delete-testimony');
    }
    /**
     * Determine whether the user can delete the model.
     */
    public function deleteAny(FilamentUser $user): bool
    {
        return $user->can('delete-testimony');
    }

    /**
     * Determine whether the user can restore the model.
     */
    public function restore(FilamentUser $user, Testimony $testimony): bool
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the model.
     */
    public function forceDelete(FilamentUser $user, Testimony $testimony): bool
    {
        //
    }
}
