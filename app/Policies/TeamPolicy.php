<?php

namespace App\Policies;

use App\Models\Team;
use App\Models\User;
use Illuminate\Auth\Access\Response;
use Chiiya\FilamentAccessControl\Models\FilamentUser;

class TeamPolicy
{
    /**
     * Determine whether the user can view any models.
     */
    public function viewAny(FilamentUser $user): bool
    {
        return $user->can('view-team');
    }

    /**
     * Determine whether the user can view the model.
     */
    public function view(FilamentUser $user, Team $team): bool
    {
        return $user->can('view-team');
    }

    /**
     * Determine whether the user can create models.
     */
    public function create(FilamentUser $user): bool
    {
        return $user->can('view-team');
    }

    /**
     * Determine whether the user can update the model.
     */
    public function update(FilamentUser $user, Team $team): bool
    {
        return $user->can('update-team');
    }

    /**
     * Determine whether the user can delete the model.
     */
    public function delete(FilamentUser $user, Team $team): bool
    {
        return $user->can('delete-team');
    }

    /**
     * Determine whether the user can delete the model.
     */
    public function deleteAny(FilamentUser $user): bool
    {
        return $user->can('delete-team');
    }

    /**
     * Determine whether the user can restore the model.
     */
    public function restore(FilamentUser $user, Team $team): bool
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the model.
     */
    public function forceDelete(FilamentUser $user, Team $team): bool
    {
        //
    }
}
