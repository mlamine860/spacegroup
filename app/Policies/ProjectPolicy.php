<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Project;
use Illuminate\Auth\Access\Response;
use Chiiya\FilamentAccessControl\Models\FilamentUser;

class ProjectPolicy
{
    /**
     * Determine whether the user can view any models.
     */
    public function viewAny(FilamentUser $user): bool
    {
        return $user->can('view-project');
    }

    /**
     * Determine whether the user can view the model.
     */
    public function view(FilamentUser $user, Project $project): bool
    {
        return $user->can('view-project');
    }

    /**
     * Determine whether the user can create models.
     */
    public function create(FilamentUser $user): bool
    {
        return $user->can('create-project');
    }

    /**
     * Determine whether the user can update the model.
     */
    public function update(FilamentUser $user, Project $project): bool
    {
        return $user->can('update-project');
    }

    /**
     * Determine whether the user can delete the model.
     */
    public function delete(FilamentUser $user, Project $project): bool
    {
        return $user->can('delete-project');
    }
    /**
     * Determine whether the user can delete the model.
     */
    public function deleteAny(FilamentUser $user): bool
    {
        return $user->can('delete-project');
    }

    /**
     * Determine whether the user can restore the model.
     */
    public function restore(FilamentUser $user, Project $project): bool
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the model.
     */
    public function forceDelete(FilamentUser $user, Project $project): bool
    {
        //
    }
}
