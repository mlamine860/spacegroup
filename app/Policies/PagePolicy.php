<?php

namespace App\Policies;

use App\Models\Page;
use Illuminate\Auth\Access\Response;
use Chiiya\FilamentAccessControl\Models\FilamentUser;

class PagePolicy
{
    /**
     * Determine whether the user can view any models.
     */
    public function viewAny(FilamentUser $user): bool
    {
        return $user->can('view-page');
    }

    /**
     * Determine whether the user can view the model.
     */
    public function view(FilamentUser $user, Page $page): bool
    {
        return $user->can('view-page');
    }

    /**
     * Determine whether the user can create models.
     */
    public function create(FilamentUser $user): bool
    {
        return $user->can('create-page');
    }

    /**
     * Determine whether the user can update the model.
     */
    public function update(FilamentUser $user, Page $page): bool
    {
        return $user->can('update-page');
    }

    /**
     * Determine whether the user can delete the model.
     */
    public function delete(FilamentUser $user, Page $page): bool
    {
        return $user->can('delete-page');
    }
    /**
     * Determine whether the user can delete the model.
     */
    public function deleteAny(FilamentUser $user): bool
    {
        return $user->can('delete-page');
    }

    /**
     * Determine whether the user can restore the model.
     */
    public function restore(FilamentUser $user, Page $page): bool
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the model.
     */
    public function forceDelete(FilamentUser $user, Page $page): bool
    {
        //
    }
}
