<?php

namespace App\Livewire\Form;

use App\Models\Message;
use Livewire\Component;
use App\Mail\MessageCreated;
use Livewire\Attributes\Rule;
use Illuminate\Support\Facades\Mail;
use Usernotnull\Toast\Concerns\WireToast;

class ContactForm extends Component
{

    use WireToast;

    #[Rule('required|min:2|max:60')]
    public string $name = '';

    #[Rule('required|email')]
    public string $email = '';

    #[Rule('required|min:12|max:20')]
    public string $phone = '';

    #[Rule('required|min:3|max:20')]
    public string $address = '';

    #[Rule('nullable|min:3|max:60')]
    public string $company = '';

    #[Rule('required|min:3|max:255')]
    public string $subject = '';

    #[Rule('required')]
    public string $message = '';

    public function save()
    {
        $validated = $this->validate();
        $message = Message::create($validated);
        try {
            Mail::to(config('site.contact_email'))->send(new MessageCreated($message));
            toast()->success('Nous vous remercions vivement de nous avoir contactés. Votre demande a bien été reçue
           et nous mettons tout en œuvre pour y répondre dans les plus brefs délais.')
           ->push();
            $this->reset();
        } catch (\Exception $exeption) {

        }


    }
    public function render()
    {
        return view('livewire.form.contact-form');
    }
}
