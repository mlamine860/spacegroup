<?php

namespace App\Livewire;

use Livewire\Component;
use Illuminate\Support\Facades\Session;

class SetLocale extends Component
{
    public function changeLocale(string $lang)
    {
        if (array_key_exists($lang, config('site.locales')) && $lang !== Session::get('lang')) {
            Session::put('lang', $lang);
            $this->js("location.reload()");
            return redirect()->back();
        }
    }
    public function render()
    {
        return view('livewire.set-locale');
    }
}
