<?php

namespace App\Livewire\Blog;

use App\Utils\SEO;
use Livewire\Component;
use Stephenjude\FilamentBlog\Models\Post as BlogPost;

class Post extends Component
{
    public BlogPost $post;
    public function render()
    {
        SEO::generate(title: $this->post->title, description: $this->post->excerpt);
        return view('livewire.blog.post');
    }

    public function mount()
    {
        $this->post->load('category', 'author', 'tags');
    }
}
