<?php

namespace App\Livewire\Blog;

use Livewire\Attributes\Url;
use Livewire\Component;
use Stephenjude\FilamentBlog\Models\Category;
use App\Models\Post;

class Blog extends Component
{
    public $category;

    #[Url( as: 'q')]
    public string $search = '';

    public function render()
    {
        $latest = Post::latest()->first();
        $categories = Category::all();
        $blogPosts = Post::filter(['category_id' => $this->category, 'search' => $this->search])->get();
        return view('livewire.blog.blog', compact('latest', 'categories', 'blogPosts'));
    }

}
