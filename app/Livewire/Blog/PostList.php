<?php

namespace App\Livewire\Blog;

use Illuminate\Database\Eloquent\Collection;
use Livewire\Component;

class PostList extends Component
{
    public function render()
    {
        return view('livewire.blog.post-list');
    }

}
