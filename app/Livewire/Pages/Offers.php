<?php

namespace App\Livewire\Pages;

use App\Utils\SEO;
use Livewire\Component;

class Offers extends Component
{
    public function render()
    {
        SEO::generate('Nos offres d’emplois');
        return view('livewire.pages.offers');
    }
}
