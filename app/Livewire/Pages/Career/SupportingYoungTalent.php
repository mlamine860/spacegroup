<?php

namespace App\Livewire\Pages\Career;

use App\Utils\SEO;
use Livewire\Component;

class SupportingYoungTalent extends Component
{
    public function render()
    {
        SEO::generate(__('pages.support.heading'));
        return view('livewire.pages.career.supporting-young-talent');
    }
}
