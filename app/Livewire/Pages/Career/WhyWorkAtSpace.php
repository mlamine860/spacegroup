<?php

namespace App\Livewire\Pages\Career;

use App\Utils\SEO;
use Livewire\Component;

class WhyWorkAtSpace extends Component
{
    public function render()
    {
        SEO::generate(__('pages.career.heading'));
        return view('livewire.pages.career.why-work-at-space');
    }
}
