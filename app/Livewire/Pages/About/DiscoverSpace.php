<?php

namespace App\Livewire\Pages\About;

use App\Utils\SEO;
use Livewire\Component;

class DiscoverSpace extends Component
{
    public function render()
    {
        SEO::generate(__('pages.discover-space.heading', ['brandName' => 'Space Group']));
        return view('livewire.pages.about.discover-space');
    }
}
