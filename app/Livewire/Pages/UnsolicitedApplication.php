<?php

namespace App\Livewire\Pages;

use App\Models\Application;
use App\Utils\SEO;
use Livewire\Component;
use Livewire\Attributes\Rule;
use Livewire\WithFileUploads;
use Illuminate\Support\Number;
use Livewire\Attributes\Computed;
use Usernotnull\Toast\Concerns\WireToast;

class UnsolicitedApplication extends Component
{
    use WithFileUploads, WireToast;


    #[Rule('required|min:2|max:60')]
    public string $first_name = '';
    #[Rule('required|min:2|max:60')]
    public string $last_name = '';

    #[Rule('required|email')]
    public string $email = '';

    #[Rule('required|min:12|max:20')]
    public string $phone_number = '';

    #[Rule('required|min:3|max:20')]
    public string $address = '';
    #[Rule('required|min:3|max:20')]
    public string $city = '';

    #[Rule('required')]
    public string $cover_letter = '';

    #[Rule('required|extensions:pdf,png,jpg|max:2048')]
    public $cv;

    #[Rule(['additional_docs.*' => 'required|extensions:pdf,png,jpg|max:4026'])]
    public $additional_docs = [];


    public function save()
    {
        $validated = $this->validate();
        $validated['cv'] = $this->cv->store('applications/cv');
        $validated['additional_docs'] = [];
        foreach ($this->additional_docs as $doc) {
            $validated['additional_docs'][] = $doc->store('applications/docs');
        }
        Application::create($validated);
        toast()->success('Merci pour avoir soumis votre candidature ! Votre CV a été reçu avec succès.')->push();
        $this->reset();
    }

    public function render()
    {
        SEO::generate(__('Unsolicited Applications'));
        return view('livewire.pages.unsolicited-application');
    }


    #[Computed]
    public function cvDisplay()
    {
        return $this->cv ? Number::fileSize($this->cv->getSize(), 2) : null;

    }

    #[Computed]
    public function docsDisplay()
    {
        if (empty($this->additional_docs))
            return;
        $size = 0;
        foreach ($this->additional_docs as $doc) {
            $size += $doc->getSize();
        }
        return Number::fileSize($size, 2);

    }
}
