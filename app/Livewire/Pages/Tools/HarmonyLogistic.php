<?php

namespace App\Livewire\Pages\Tools;

use App\Utils\SEO;
use Livewire\Component;

class HarmonyLogistic extends Component
{
    public function render()
    {
        SEO::generate('Harmonie logistique  LOT1');
        return view('livewire.pages.tools.harmony-logistic');
    }
}
