<?php

namespace App\Livewire\Pages\Tools;

use App\Utils\SEO;
use Livewire\Component;

class HarmonyTracking extends Component
{
    public function render()
    {
        SEO::generate('Harmonie tracking LOT1');
        return view('livewire.pages.tools.harmony-tracking');
    }
}
