<?php

namespace App\Livewire\Pages;

use App\Models\Page;
use Livewire\Component;
use App\Utils\SEO;

class PrivacyPolicy extends Component
{
    public function render()
    {
        $page = Page::whereSlug('politique-de-confidentialite')->firstOrFail();
        SEO::generate($page->title);
        return view('livewire.pages.privacy-policy', compact('page'));
    }
}
