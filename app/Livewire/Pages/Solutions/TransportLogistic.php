<?php

namespace App\Livewire\Pages\Solutions;

use App\Utils\SEO;
use Livewire\Component;

class TransportLogistic extends Component
{
    public function render()
    {
        SEO::generate(__('pages.transport-logistics.heading'));
        return view('livewire.pages.solutions.transport-logistic');
    }
}
