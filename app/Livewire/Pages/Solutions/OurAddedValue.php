<?php

namespace App\Livewire\Pages\Solutions;

use App\Utils\SEO;
use Livewire\Component;

class OurAddedValue extends Component
{
    public function render()
    {
        SEO::generate(__('pages.our-add-values.heading'));
        return view('livewire.pages.solutions.our-added-value');
    }
}
