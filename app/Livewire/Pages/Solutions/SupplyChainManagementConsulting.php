<?php

namespace App\Livewire\Pages\Solutions;

use App\Utils\SEO;
use Livewire\Component;

class SupplyChainManagementConsulting extends Component
{
    public function render()
    {
        SEO::generate( __('pages.supply-chain-managment-consulting.heading'));
        return view('livewire.pages.solutions.supply-chain-management-consulting');
    }
}
