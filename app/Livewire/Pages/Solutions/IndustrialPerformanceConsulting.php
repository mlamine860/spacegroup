<?php

namespace App\Livewire\Pages\Solutions;

use App\Utils\SEO;
use Livewire\Component;

class IndustrialPerformanceConsulting extends Component
{
    public function render()
    {
        SEO::generate(__('Industrial performance consulting'));
        return view('livewire.pages.solutions.industrial-performance-consulting');
    }
}
