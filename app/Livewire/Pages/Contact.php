<?php

namespace App\Livewire\Pages;

use App\Utils\SEO;
use Livewire\Component;

class Contact extends Component
{
    public function render()
    {
        SEO::generate(__('Email Us'));
        return view('livewire.pages.contact');
    }
}
