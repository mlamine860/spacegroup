<?php

namespace App\Livewire;

use App\Utils\SEO;
use Livewire\Component;

class Home extends Component
{

    public function render()
    {

        SEO::generate();
        return view('livewire.home');
    }
}
