<?php

namespace App\Models;

use Chiiya\FilamentAccessControl\Models\FilamentUser;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Page extends Model
{
    use HasFactory;


    protected $fillable = [
        'title',
        'slug',
        'user_id',
        'content',
    ];


    public function user(): BelongsTo
    {
        return $this->belongsTo(FilamentUser::class, 'user_id');
    }
}
