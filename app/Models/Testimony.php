<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use App\Utils\UIAvatar;

class Testimony extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'job',
        'avatar',
        'text',
        'stars',
    ];

    public function avatarUrl(): Attribute
    {
        return Attribute::make(get: function ($value) {
            return Storage::exists($this->avatar) ? Storage::url($this->avatar) : UiAvatar::getAvatar($this);
        });
    }
}
