<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Post extends \Stephenjude\FilamentBlog\Models\Post
{
    use HasFactory;

    public function scopeFilter(Builder $query, array $filters = [])
    {
        $query->when($filters['category_id'], fn($query) => $query->where('blog_category_id', $filters['category_id']))
            ->when($filters['search'], fn($query) => $query->where('title', 'LIKE', '%'.$filters['search'] . '%'));

    }

}