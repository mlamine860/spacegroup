<?php

/**
 * Web site main setting
 */

return [
    'name' => 'Space Group',
    'url' => 'https://spacegroup-gn.com',
    'contact_email' => 'contact@spacegroup-gn.com',
    'address' => '988 Myrtle Ave. Providence, RI 02904',
    'main_menu' => [

        [
            'label' => 'Home',
            'url' => '/'
        ],
        [
            'label' => 'Our Solutions',
            'url' => '/solutions',
            'child' => [
                [
                    'label' => 'Transport and logistics',
                    'url' => '/solutions/transport-logistics',
                    'child' => [],
                ],
                [
                    'label' => 'Supply Chain Management Consulting',
                    'url' => '/solutions/supply-chain-managment-consulting',
                    'child' => [],
                ],
                [
                    'label' => 'Industrial performance consulting',
                    'url' => '/solutions/industrial-performance-consulting',
                    'child' => [],
                ],
                [
                    'label' => 'Our Added Values',
                    'url' => '/solutions/our-added-values',
                    'child' => [],
                ]
            ]
        ],
        [
            'label' => 'Who are we?',
            'url' => '/about-us',
            'child' => [
                [
                    'label' => 'Discover Space',
                    'url' => '/discover-space',
                    [],
                ],
                // [
                //     'label' => 'Our History',
                //     'url' => '/our-history',
                //     [],
                // ],
                // [
                //     'label' => 'Our Vision',
                //     'url' => '/our-vision',
                //     [],
                // ],
                // [
                //     'label' => 'Our Goals',
                //     'url' => '/our-goals',
                //     [],
                // ],
                // [
                //     'label' => 'Our Values',
                //     'url' => '',
                //     [],
                // ],

            ]
        ],
        [
            'label' => 'Career',
            'url' => '/career',
            'child' => [
                [
                    'label' => 'Why Work At Space',
                    'url' => '/why-work-at-space',
                    'child' => []
                ],
                [
                    'label' => 'Our Job Offers',
                    'url' => '/offers',
                    'child' => []
                ],
                [
                    'label' => 'Supporting Young Talents',
                    'url' => '/supporting-young-talents',
                    'child' => []
                ],
                [
                    'label' => 'Unsolicited Applications',
                    'url' => '/unsolicited-application',
                    'child' => []
                ],
            ]
        ],
        // [
        //     'label' => 'Innovation',
        //     'url' => '',
        // ],
        [
            'label' => 'My Harmony space',
            'url' => '',
            'child' => [
                [
                    'label' => 'My Harmonie Logistics Space',
                    'url' => 'https://harmonie.spacegroup-gn.com',
                    'external' => true,
                    'child' => []
                ],
                [
                    'label' => 'My Harmony Tracking Space',
                    'url' => 'https://tracking.spacegroup-gn.com',
                    'external' => true,
                    'child' => []
                ],
            ]
        ],
        // [
        //     'label' => 'News',
        //     'url' => '/news',
        // ],
    ],
    'locales' => [
        'fr' => 'French',
        'en' => 'English',
    ],
    'seo' => [
        'title' => 'Logistics, Transport, Supply Chain and Industrial Performance - Space Group',
        'description' => 'Optimize your supply chain with our logistics, transport, supply chain and industrial performance improvement services. Integrated solutions for the growth of your business.',
        'keywords' => 'logistics, transport, supply chain, industrial performance, supply chain, logistics services, transport solutions, performance improvement',
        'geo_placename' => 'Conakry, Guinée',
        'geo_region' => 'GN',
        'geo_position' => '9.509167;-13.712222',
        'property' => [
            'site_name' => 'Spacegroup.com',
            'language' => 'fr'

        ]
    ]
];

