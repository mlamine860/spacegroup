<div class="flex items-center gaxp-2">
    @for ($i = 1; $i <= 5; $i++)
        @if ($stars >= $i)
            <x-icons.star class="w-5 h-5" />
        @else
            <x-icons.star-stroke class="w-5 h-5" />
        @endif
    @endfor
</div>
