@props(['name', 'iconClass' => ''])
<div {{ $attributes->merge([
    'class' => 'flex items-center justify-center rounded-full mr-2',
]) }}>
    <x-dynamic-component :component="$name" class="{{$iconClass}}" />
</div>
