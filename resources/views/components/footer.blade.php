<footer class="bg-secondary-800" aria-label="{{ __('Footer') }}">
    <div class="bg-secondary-500">
        <div class="container flex flex-col md:flex-row md:items-center md:justify-between gap-4">
            <a href="{{ route('home') }}" class="w-full h-full bg-secondar-700 block  py-4">
                <x-application-mark />
            </a>
            <div class="hidden md:flex justify-center items-center md:pr-6 w-full h-full  section-title text-white">
                {{ __('Pages') }}</div>
            <div class="hidden md:flex justify-center items-center md:pr-4 w-full h-full  section-title text-white">
                {{ __('Tools') }}</div>
            <div
                class="hidden md:flex justify-center items-center md:justify-start w-full h-full  section-title text-white">
                {{ __('Subscribe') }}</div>
        </div>
    </div>

    <div class="container grid grid-cols-1 md:grid-cols-4 gap-6 py-8 text-white">
        <div class="space-y-4 flex-1">
            <p class="text-medium">
                {{ __('We think like engineers and act like entrepreneurs, to invent tailor-made solutions every day') }}
            </p>
            <ul class="mt-6 flex gap-x-2">
                <li>
                    <a href="https://www.facebook.com/people/Space-Group/100085852627889/?mibextid=qWsEUC">
                        <x-icon-button class="bg-secondary-500/50 w-10 h-10">
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"
                                class="w-8 h-8 text-secondary-200 hover:text-secondary-300 transition"
                                fill="currentColor">
                                <path
                                    d="M12.001 2C6.47813 2 2.00098 6.47715 2.00098 12C2.00098 16.9913 5.65783 21.1283 10.4385 21.8785V14.8906H7.89941V12H10.4385V9.79688C10.4385 7.29063 11.9314 5.90625 14.2156 5.90625C15.3097 5.90625 16.4541 6.10156 16.4541 6.10156V8.5625H15.1931C13.9509 8.5625 13.5635 9.33334 13.5635 10.1242V12H16.3369L15.8936 14.8906H13.5635V21.8785C18.3441 21.1283 22.001 16.9913 22.001 12C22.001 6.47715 17.5238 2 12.001 2Z">
                                </path>
                            </svg>
                        </x-icon-button>
                    </a>
                </li>
                <li>
                    <a href="https://www.linkedin.com/company/groupspaceguin%C3%A9e">
                        <x-icon-button class="bg-secondary-500/50 w-10 h-10">
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"
                                class="w-8 h-8 text-secondary-200 hover:text-secondary-300 transition"
                                fill="currentColor">
                                <path
                                    d="M6.94048 4.99993C6.94011 5.81424 6.44608 6.54702 5.69134 6.85273C4.9366 7.15845 4.07187 6.97605 3.5049 6.39155C2.93793 5.80704 2.78195 4.93715 3.1105 4.19207C3.43906 3.44699 4.18654 2.9755 5.00048 2.99993C6.08155 3.03238 6.94097 3.91837 6.94048 4.99993ZM7.00048 8.47993H3.00048V20.9999H7.00048V8.47993ZM13.3205 8.47993H9.34048V20.9999H13.2805V14.4299C13.2805 10.7699 18.0505 10.4299 18.0505 14.4299V20.9999H22.0005V13.0699C22.0005 6.89993 14.9405 7.12993 13.2805 10.1599L13.3205 8.47993Z">
                                </path>
                            </svg>
                        </x-icon-button>
                    </a>
                </li>
            </ul>
        </div>

        <ul class="flex-1 flex flex-col md:items-center">
            <li class="md:hidden text-xl font-medium mb-2">{{ __('Pages') }}</li>
            {{-- <li>
                <a href="/innovation" wire:navigate
                    class="text-gray-200 hover:text-secondary-200/80 transition">{{ __('Innovation') }}</a>
            </li> --}}
            <li>
                <a href="{{ route('pages.about') }}" wire:navigate
                    class="text-gray-200 hover:text-secondary-200/80 transition">{{ __('About Us') }}</a>
            </li>
            <li>
                <a href="{{ route('pages.contact') }}" wire:navigate
                    class="text-gray-200 hover:text-secondary-200/80 transition">{{ __('Contact') }}</a>
            </li>
            {{-- <li>
                <a href="" wire:navigate
                    class="text-gray-200 hover:text-secondary-200/80 transition">{{ __('Our Projects') }}</a>
            </li>
            --}}
            <li>
                <a href="{{ route('blog') }}" wire:navigate
                    class="text-gray-200 hover:text-secondary-200/80 transition">{{ __('News') }}</a>
            </li>
        </ul>

        <ul class="flex-1 flex flex-col md:items-center">
            <li class="md:hidden text-xl font-medium mb-2">{{ __('Utility') }}</li>
            <li>
                <a href="{{ url('https://harmonie.spacegroup-gn.com') }}"
                    class="text-gray-200  hover:text-secondary-200/80 transition">{{ __('Harmony Logistics') }}</a>
            </li>
            <li>
                <a href="{{ url('https://tracking.spacegroup-gn.com') }}"
                    class="text-gray-200  hover:text-secondary-200/80 transition">{{ __('Harmony Tracking') }}</a>
            </li>
        </ul>

        {{-- Newsletters --}}
        <div class="flex-1 flex flex-col items-start gap-y-4">
            <p class="md:hidden text-xl font-medium">{{ __('Subscribe') }}</p>
            <x-form.input name="email" class="text-white py-3 w-full" placeholder="{{ __('Email*') }}" />
            <x-button class="px-6 py-4">{{ __('Subscribe') }}</x-button>
        </div>
    </div>

    <div class="h-px w-full bg-secondary-300/20"></div>
    <div class="py-8">
        <div class="container">
            <p class="text-sm text-white">
                Copyright © Spacegroup | {{ __('All rights reserved.') }}
            </p>
        </div>
    </div>
    <div x-data="{ showScrollTopBtn: false }" @scroll.window="showScrollTopBtn = window.scrollY >= 300">
        <button @click="window.scrollTo({top: 0, left: 0, behavior: 'smooth'})" aria-hidden="showSccrollTopBtn"
            x-show="showScrollTopBtn" x-transition
            class="w-8 h-8 fixed bottom-2 right-4 lg:right-2 bg-secondary-600 flex items-center justify-center shadow-xl cursor-pointer">
            <x-icons.chevron-up class="text-secondary-200 w-6 h-6" />
        </button>
    </div>
</footer>
