<div
    {{ $attributes->merge([
        'class' =>
            'h-8 w-8 bg-transparent border-4 border-secondary-200 border-l-secondary-50 border-t-secondary-50  rounded-full animate-spin',
    ]) }}>
</div>
