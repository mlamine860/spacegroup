<nav class="hidden lg:block">
    <ul class="flex items-center space-x-6 py-2">
        @foreach (config('site.main_menu') as $menu)
            <li class="py-3 group relative">
                @if (isset($menu['child']))
                    <div class="text-gray-300 font-medium hover:text-secondary-200/80 transition cursor-pointer">
                        <div class="flex items-center">
                            <span class="font-bold">{{ __($menu['label']) }}</span>
                            <x-icons.chevron-down class="ml-1 w-3 h-3" />
                        </div>
                        <div class="absolute top-12 -right-10 hidden lg:group-hover:block z-10 w-60 origin-top-right rounded-md shadow-xl border border-secondary-400/30 bg-secondary-500"
                             role="dropdown" aria-orientation="vertical">
                            <div class="py-4" role="none">
                                @foreach ($menu['child'] as $childMenu)
                                    <a href="{{url($childMenu['url']) }}" {{ !isset($childMenu['external']) ? 'wire:navigate' : 'target=blank'}}
                                       class="text-gray-200 text-sm block px-3 py-1 hover:text-secondary-200/80 transition">
                                        {{ __($childMenu['label']) }}</a>
                                @endforeach
                            </div>

                        </div>
                    </div>
                @else
                    <a href="{{url($menu['url'])}}" wire:navigate
                       class="text-gray-300 font-medium hover:text-secondary-200/80 transition">
                        <span class="font-bold">{{ __($menu['label']) }}</span>
                    </a>
                @endif
            </li>
        @endforeach
        <li>
            <div class="relative inline-block text-left" x-data="{ openLangBox: false }">
                <button type="button" @click="openLangBox = !openLangBox"
                        class="inline-flex w-full  justify-center gap-x-1.5 rounded-md px-3 py-2 text-sm  text-gray-300 font-medium  transition shadow-sm ring-secondary-200 ring-1 ring-inset"
                        id="menu-button" aria-expanded="true" aria-haspopup="true">
                    {{ __(config('app.locale')) }}
                    <svg class="-mr-1 h-5 w-5 text-gray-300" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                        <path fill-rule="evenodd"
                              d="M5.23 7.21a.75.75 0 011.06.02L10 11.168l3.71-3.938a.75.75 0 111.08 1.04l-4.25 4.5a.75.75 0 01-1.08 0l-4.25-4.5a.75.75 0 01.02-1.06z"
                              clip-rule="evenodd" />
                    </svg>
                </button>
                <div @click.outside="openLangBox = false" x-show="openLangBox" @mouseleave="openLangBox = false"
                     class="absolute right-0 z-10 mt-2 w-28 origin-top-right rounded-md bg-secondary-500 shadow-lg ring-1 ring-black ring-opacity-5 focus:outline-none"
                     role="menu" aria-orientation="vertical" aria-labelledby="menu-button" tabindex="-1">
                    <livewire:set-locale />
                </div>
            </div>

        </li>
    </ul>
</nav>
