<h3 {{$attributes->merge([
    'class' => 'font-medium  uppercase title border-l-4 border-primary-500 pl-2'
])}}>
    {{$slot}}
</h3>
