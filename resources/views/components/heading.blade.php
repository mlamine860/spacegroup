<header {{ $attributes->merge([
    'class' => 'mt-[444px] lg:mt-[100px]',
]) }}>
    {{ $slot }}
</header>
