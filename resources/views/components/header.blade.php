<header id="header" class="absolute left-0 top-0 w-full z-20 bg-transparent" x-data="{ openMenu: false }">
    <div class="bg-secondary-600 py-4 w-full">
        <div class="container space-y-4 lg:space-y-0 lg:flex justify-between items-center">
            <a href="{{ route('home') }}" wire:navigate class="flex items-center"
                title="Logo de Space Group - Solutions Logistiques, Transport, Supply Chain et Performance Industrielle">
                <x-application-mark />
            </a>
            <ul class="mt-6 space-y-2 lg:flex">

                <li class="flex items-center">
                    <x-buttons.circular-icon-button
                        class="text-secondary-200 h-10 w-10 bg-secondary-500/50 border border-secondary-400/40"
                        iconClass="w-6 h-6" name="icons.transport" />
                    <a href="{{ url('https://tracking.spacegroup-gn.com') }}" target="blank"
                        class="text-gray-200 font-medium hover:text-secondary-200/80 transition lg:mr-4">
                        {{ __('Follow My Packages') }}
                    </a>
                </li>
                <li class="flex items-center">
                    <x-buttons.circular-icon-button
                        class="text-secondary-200 h-10 w-10 bg-secondary-500/50 border border-secondary-400/40"
                        iconClass="w-6 h-6" name="icons.user" />
                    <a href="{{ url('https://harmonie.spacegroup-gn.com') }}" target="blank"
                        class="text-gray-200 font-medium hover:text-secondary-200/80 transition lg:mr-4">
                        {{ __('Customer Resources') }}
                    </a>
                </li>
                <li class="flex items-center">
                    <x-buttons.circular-icon-button
                        class="text-secondary-200 h-10 w-10 bg-secondary-500/50 border border-secondary-400/40"
                        iconClass="w-6 h-6" name="icons.whatsapp" />
                    <a href=" https://wa.me/+224611335050"
                        class="text-gray-200 font-medium hover:text-secondary-200/80 transition lg:mr-4">
                        Whatsapp
                    </a>
                </li>
                <li class="flex items-center">
                    <x-buttons.circular-icon-button
                        class="text-secondary-200 h-10 w-10 bg-secondary-500/50 border border-secondary-400/40"
                        iconClass="w-6 h-6" name="icons.phone" />
                    <a href="tel:+224611335050"
                        class="text-gray-200  font-medium hover:text-secondary-200/80 transition lg:mr-4">
                        +224 611 335 050
                    </a>
                </li>
                <li class="flex items-start">
                    <div class="flex flex-col justify-center lg:mt-1 ml-2">
                        <button aria-label="__('Dark mode toggler')"
                            @click.parent="darkMode = !darkMode; localStorage.setItem('theme', darkMode ? 'dark' : 'light')"
                            class="relative cursor-pointer p-2" for="light-switch">
                            <svg :aria-hidden="darkMode" class="dark:hidden fill-secondary-200" width="16"
                                height="16" xmlns="http://www.w3.org/2000/svg">
                                <path
                                    d="M7 0h2v2H7zM12.88 1.637l1.414 1.415-1.415 1.413-1.413-1.414zM14 7h2v2h-2zM12.95 14.433l-1.414-1.413 1.413-1.415 1.415 1.414zM7 14h2v2H7zM2.98 14.364l-1.413-1.415 1.414-1.414 1.414 1.415zM0 7h2v2H0zM3.05 1.706 4.463 3.12 3.05 4.535 1.636 3.12z" />
                                <path d="M8 4C5.8 4 4 5.8 4 8s1.8 4 4 4 4-1.8 4-4-1.8-4-4-4Z" />
                            </svg>
                            <svg :aria-hidden="darkMode" class="hidden dark:block fill-secondary-200" width="16"
                                height="16" xmlns="http://www.w3.org/2000/svg">
                                <path
                                    d="M6.2 1C3.2 1.8 1 4.6 1 7.9 1 11.8 4.2 15 8.1 15c3.3 0 6-2.2 6.9-5.2C9.7 11.2 4.8 6.3 6.2 1Z" />
                                <path
                                    d="M12.5 5a.625.625 0 0 1-.625-.625 1.252 1.252 0 0 0-1.25-1.25.625.625 0 1 1 0-1.25 1.252 1.252 0 0 0 1.25-1.25.625.625 0 1 1 1.25 0c.001.69.56 1.249 1.25 1.25a.625.625 0 1 1 0 1.25c-.69.001-1.249.56-1.25 1.25A.625.625 0 0 1 12.5 5Z" />
                            </svg>
                            <span class="sr-only">Switch to light / dark version</span>
                        </button>
                    </div>
                </li>
            </ul>
        </div>
    </div>
    {{-- Desktop Navigation --}}
    <div class="bg-secondary-500 py-2 lg:py-0">
        <div class="container flex items-center justify-between">
            <x-desktop-menu />
            <a href="{{ route('pages.contact') }}" wire:navigate
                class="button px-6 py-6 rounded-none">{{ __('Request a quote') }}</a>
            <ul class="lg:hidden">
                <li>
                    <div class="relative inline-block text-left" x-data="{ openLangBox: false }">
                        <button type="button" @click="openLangBox = !openLangBox"
                            class="inline-flex w-full  justify-center gap-x-1.5 rounded-md px-3 py-2 text-sm  text-gray-300 font-medium  transition shadow-sm ring-secondary-200 ring-1 ring-inset"
                            id="menu-button" aria-expanded="true" aria-haspopup="true">
                            {{ __(config('app.locale')) }}
                            <svg class="-mr-1 h-5 w-5 text-gray-300" viewBox="0 0 20 20" fill="currentColor"
                                aria-hidden="true">
                                <path fill-rule="evenodd"
                                    d="M5.23 7.21a.75.75 0 011.06.02L10 11.168l3.71-3.938a.75.75 0 111.08 1.04l-4.25 4.5a.75.75 0 01-1.08 0l-4.25-4.5a.75.75 0 01.02-1.06z"
                                    clip-rule="evenodd" />
                            </svg>
                        </button>
                        <div @click.outside="openLangBox = false" x-show="openLangBox" @mouseleave="openLangBox = false"
                            class="absolute right-0 z-10 mt-2 w-28 origin-top-right rounded-md bg-secondary-500 shadow-lg ring-1 ring-black ring-opacity-5 focus:outline-none"
                            role="menu" aria-orientation="vertical" aria-labelledby="menu-button" tabindex="-1">
                            <livewire:set-locale />
                        </div>
                    </div>

                </li>
            </ul>
            <button @click="openMenu = !openMenu"
                class="lg:hidden bg-secondary-700 rounded-md p-4 flex items-center justify-center">
                <x-icons.menu class="w-6 h-6 text-gray-200" />
            </button>
        </div>
    </div>
    {{-- Mobile Navigation --}}
    <div class="lg:hidden bg-secondary-700 max-h-0 overflow-hidden duration-500"
        :class="openMenu ? 'max-h-screen duration-500 py-4' : ''">
        <div class="container">
            <x-mobile-nav />
        </div>
    </div>
</header>
