@props(['disabled' => false])

<textarea {{ $disabled ? 'disabled' : '' }} {!! $attributes->merge([
    'class' =>
        'border-secondary-200 dark:border-secondary-300 bg-transparent text-white focus:border-secondary-300  focus:ring-seconary-500 dark:focus:ring-indigo-600 rounded-md shadow-sm',
]) !!}>
{{$slot}}
</textarea>
