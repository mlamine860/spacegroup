@props(['disabled' => false, 'error' => false])

<input {{ $disabled ? 'disabled' : '' }} {!! $attributes->merge([
    'class' =>
        'border-secondary-200  bg-transparent text-white dark:text-white focus:border-secondary-300  focus:ring-seconary-400  rounded-md shadow-sm',
]) !!}>
