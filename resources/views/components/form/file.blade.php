@props(['disabled' => false, 'name' => '', 'label' => '', 'required' => true, 'size' => null])
<div class="text-white">
    <div class="flex items-center mb-1">{{ $label }}
        @if ($required)
            <span class="text-red-400">*</span>
        @else
            <span class="ml-2 text-slate-200 text-sm opacity-70">({{ __('Optional') }})</span>
        @endif
    </div>
    <div for="{{ $name }}"
        class="pl-2 border-secondary-200  border overflow-hidden  bg-transparent text-white flex items-center justify-between focus:border-secondary-300  focus:ring-seconary-400  rounded-md shadow-sm">

        <input class="w-0 h-0 opacity-0" type="file" {{ $disabled ? 'disabled' : '' }} id="{{ $name }}"
            {{ $attributes() }}>
        <label for="{{ $name }}" class="flex items-center gap-x-1 bg-secondary-300 p-[10px] cursor-pointer">
            <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5"
                stroke="currentColor" class="w-6 h-6">
                <path stroke-linecap="round" stroke-linejoin="round"
                    d="M12 9.75v6.75m0 0-3-3m3 3 3-3m-8.25 6a4.5 4.5 0 0 1-1.41-8.775 5.25 5.25 0 0 1 10.233-2.33 3 3 0 0 1 3.758 3.848A3.752 3.752 0 0 1 18 19.5H6.75Z" />
            </svg>
            {{ $size  ? $size :  __('Browse') }}
        </label>
    </div>
</div>

