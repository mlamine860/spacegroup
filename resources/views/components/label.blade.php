@props(['value'])

<label {{ $attributes->merge(['class' => 'block font-medium  text-gray-700 dark:text-gray-300']) }}>
    {{ $value ?? $slot }}@isset($value)<span class="text-xs text-primary-300">*</span>@endisset
</label>
