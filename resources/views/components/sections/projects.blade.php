<section class="py-16" id="projects" aria-label="{{ __('Projects') }}">
    <div class="container">
        <h2 class="text-center section-title mb-8">{{ __('Transporting Across The World') }}</h2>
        <div class="grid grid-cols-1 sm:grid-cols-2 md:grd-cols-3 lg:grid-cols-5 gap-6">
            <div class="flex-1 relative">
                <div class="absolute bottom-0 left-0 right-0 bg-black/80 p-3 space-y-1">
                    <h3 class="text-lg font-bold text-gray-100">{{ __('Liquid Transportation') }}</h3>
                    <p class="text-sm text-primary-500 font-medium">{{ __('Premium Tankers') }}</p>
                </div>
                <img src="{{ asset('images/liquid-trucker.jpg') }}" class="w-full max-h-96 object-cover"
                    alt="{{ __('Liquid truck') }}" title="{{ __('Liquid truck') }}">
            </div>
            <div class="flex-1 relative">
                <div class="absolute bottom-0 left-0 right-0 bg-black/80 p-3 space-y-1">
                    <h3 class="text-lg font-bold text-gray-100">{{ __('Contract Logistics') }}</h3>
                    <p class="text-sm text-primary-500 font-medium">{{ __('Road Transportation') }}</p>
                </div>
                <img src="{{ asset('images/contract-logistic.jpg') }}" class="w-full max-h-96 object-cover"
                    alt="{{ __('Contract Logistics') }}" title="{{ __('Contract Logistics') }}">
            </div>
            <div class="flex-1 relative">
                <div class="absolute bottom-0 left-0 right-0 bg-black/80 p-3 space-y-1">
                    <h3 class="text-lg font-bold text-gray-100">{{ __('Specialized Transport') }}</h3>
                    <p class="text-sm text-primary-500 font-medium">{{ __('Ocean Transports') }}</p>
                </div>
                <img src="{{ asset('images/ocean-transport.jpg') }}" class="w-full max-h-96 object-cover"
                    alt="{{ _('Specialized Transport') }}" title="{{ _('Specialized Transport') }}">
            </div>
            <div class="flex-1 relative">
                <div class="absolute bottom-0 left-0 right-0 bg-black/80 p-3 space-y-1">
                    <h3 class="text-lg font-bold text-gray-100">{{ __('Packaging Solutions') }}</h3>
                    <p class="text-sm text-primary-500 font-medium">{{ __('Warehouse Management') }}</p>
                </div>
                <img src="{{ asset('images/packing-solution.jpg') }}" class="w-full max-h-96 object-cover"
                    alt="{{ __('Packaging Solutions') }}" title="{{ __('Packaging Solutions') }}">
            </div>
            <div class="flex-1 relative">
                <div class="absolute bottom-0 left-0 right-0 bg-black/80 p-3 space-y-1">
                    <h3 class="text-lg font-bold text-gray-100">{{ __('Warehouse & Distribution') }}</h3>
                    <p class="text-sm text-primary-500 font-medium">{{ __('Large Warehouse') }}</p>
                </div>
                <img src="{{ asset('images/warehouse-distribution.jpg') }}" class="w-full max-h-96 object-cover"
                    alt="{{ __('Warehouse & Distribution') }}" title="{{ __('Warehouse & Distribution') }}">
            </div>
        </div>
    </div>
    <div
        class="w-full h-[40vh] bg-gradient-to-tr from-secondary-700 to-secondary-400 -mt-16 flex items-center justify-center">
        <x-button-link href="#" class="px-6 py-4 mt-4 bg-gradient-to-r from-secondary-500 to-secondary-300 hover:from-secondary-600 hover:to-secondary-400 transition rounded">{{ __('More work') }}</x-button-link>
    </div>
</section>
