<section class="pt-32 pb-16" id="solutions" aria-label="{{ __('Solutions') }}">
    <div class="container">
        <div class="space-y-2 pb-12">
            <x-subtitle class="text-xs bg-secondary-600/5 dark:bg-white/5 inline-block pr-1 ">
                {{ __('What We Do') }}
            </x-subtitle>
            <h2 class="title text-3xl font-bold">
                {{ __('Safe & Reliable Cargo Solutions') }}</h2>
        </div>
        <div class="grid grid-cols-1 md:grid-cols-2 gap-x-8 gap-y-16">
            <div class="flex items-start flex-col lg:flex-row lg:gap-x-6 gap-2">
                <x-icons.transport class="w-16 h-16 text-primary-500 dark:text-primary-500" />
                <div class="hidden lg:block h-44 w-px bg-slate-200"></div>
                <div class="flex-1 space-y-2">
                    <h2 class="section-title text-xl hover:text-primary-500">
                        <a href="{{ route('pages.transport-logistics') }}" wire:navigate>{{ __('Transport') }}</a>
                    </h2>
                    <p class="text-slate-500 dark:text-slate-200 leading-relaxed">
                        {{ __("SPACE GROUP offre des solutions de transport personnalisées et des flottes de
                                                    camions adaptées à divers secteurs d'activité. Que vous acheminiez des matières
                                                    premières, des produits dangereux, des engins lourds, des pièces détachées, des
                                                    produits finis, ou autre, nous vous aidons à optimiser vos opérations de manière efficace.
                                                    Notre système informatique interne, HARMONIE, offre une visibilité en temps réel de
                                                    votre marchandise.") }}
                    </p>
                </div>
            </div>
            <div class="flex items-start flex-col lg:flex-row lg:gap-x-6 gap-2">
                <x-icons.supply-chain class="w-16 h-16 text-primary-500 dark:text-primary-500" />
                <div class="hidden lg:block h-44 w-px bg-slate-200"></div>
                <div class="flex-1 space-y-2">
                    <h2 class="section-title text-xl hover:text-primary-500">
                        <a href=""
                            wire:navigate>{{ __('Logistique & Supply Chain') }}</a>
                    </h2>
                    <p class="text-slate-500 dark:text-slate-200 leading-relaxed">
                        {{ __('We optimize your logistics through continuous innovation, integrating your specific objectives and challenges. Discover our comprehensive range of logistics and supply chain management solutions, tailor-made to optimize the efficiency of your operations and ensure seamless fluidity in your supply chain.') }}
                    </p>
                </div>
            </div>
            <div class="flex items-start flex-col lg:flex-row lg:gap-x-6 gap-2">
                <x-icons.consulting class="w-16 h-16 text-primary-500 dark:text-primary-500" />
                <div class="hidden lg:block h-44 w-px bg-slate-200"></div>
                <div class="flex-1 space-y-2">
                    <h2 class="section-title text-xl hover:text-primary-500">
                        <a href="{{ route('pages.supply-chain-managment-consulting') }}"
                            wire:navigate>{{ __('Conseil en Supply Chain Management') }}</a>
                    </h2>
                    <p class="text-slate-500 dark:text-slate-200 leading-relaxed">
                        {{ __("SPACE GROUP's Supply Chain and Operations consulting expertise proactively responds to complex challenges and opportunities to develop and secure your operations. Starting from your global strategy, we support you in redefining your supply chain and operations, in line with your company's strategic objectives.") }}
                    </p>
                </div>
            </div>
            <div class="flex items-start flex-col lg:flex-row lg:gap-x-6 gap-2">
                <x-icons.chart-performance class="w-16 h-16 text-primary-500 dark:text-primary-500" />
                <div class="hidden lg:block h-44 w-px bg-slate-200"></div>
                <div class="flex-1 space-y-2">
                    <h2 class="section-title text-xl hover:text-primary-500">
                        <a href="{{ route('pages.industrial-performance-consulting') }}"
                            wire:navigate>{{ __('Conseil en performance industrielle') }}</a>
                    </h2>
                    <p class="text-slate-500 dark:text-slate-200 leading-relaxed">
                        {{ __('Explore our comprehensive offering of industrial performance services, dedicated to operational optimization, process improvement and the sustained growth of your business. Our integrated approach, from design to implementation, aims to maximize operational efficiency and strengthen competitiveness in the industrial marketplace through our commitment to excellence.') }}
                    </p>
                </div>
            </div>

        </div>
    </div>
</section>
