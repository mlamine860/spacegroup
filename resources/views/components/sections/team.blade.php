<section class="pt-16 pb-32" id="team" aria-label="{{__('Team')}}">
    <div class="container">
        <div class="space-y-2 flex flex-col">
            <x-subtitle class="text-xs bg-secondary-600/5 dark:bg-white/5 w-max pr-1">{{ __('The Transporters') }}</x-subtitle>
            <h2 class="section-title">{{__('Meet Expert Team')}}</h2>
        </div>
        <div class="grid grid-cols-1 sm:grid-cols-2 md:grid-cols-3 gap-6 pt-10">
            <div>
                <img src="{{ asset('images/lamine-bah.jpg') }}" alt="Lamine Bah" class="w-full h-96">
                <div class="bg-secondary-500 p-2 space-y-1 relative">
                    <h3 class="title text-lg font-medium text-white">Lamine Bah</h3>
                    <p class="text-secondary-300 text-sm">{{ __('Transport Manager') }}</p>
                    <div
                        class="absolute -top-5 right-0 flex gap-x-1 bg-white dark:bg-secondary-600 p-4">
                        <a href="#" class="hover:-translate-y-1 transition duration-500"><x-icons.facebook class="w-8 h-8 text-secondary-500 dark:text-white" /></a>
                        <a href="#" class="hover:-translate-y-1 transition duration-500"><x-icons.linked-in class="w-8 h-8 text-secondary-500 dark:text-white" /></a>
                        <a href="#" class="hover:-translate-y-1 transition duration-500"><x-icons.instagram class="w-8 h-8 text-secondary-500 dark:text-white" /></a>
                    </div>
                </div>
            </div>
            <div>
                <img src="{{ asset('images/user-1.jpg') }}" alt="John Doe" class="h-96 w-full">
                <div class="bg-secondary-500 p-2 space-y-1 relative">
                    <h3 class="title text-lg font-medium text-white">John Doe</h3>
                    <p class="text-secondary-300 text-sm">{{ __('Warehouse Head') }}</p>
                    <div
                        class="absolute -top-5 right-0 flex gap-x-1 bg-white dark:bg-secondary-600 p-4">
                        <a href="#" class="hover:-translate-y-1 transition duration-500"><x-icons.facebook class="w-8 h-8 text-secondary-500 dark:text-white" /></a>
                        <a href="#" class="hover:-translate-y-1 transition duration-500"><x-icons.linked-in class="w-8 h-8 text-secondary-500 dark:text-white" /></a>
                        <a href="#" class="hover:-translate-y-1 transition duration-500"><x-icons.instagram class="w-8 h-8 text-secondary-500 dark:text-white" /></a>
                    </div>
                </div>
            </div>
            <div>
                <img src="{{ asset('images/user-2.jpg') }}" alt="Hassane Barry" class="w-full h-96">
                <div class="bg-secondary-500 p-2 space-y-1 relative">
                    <h3 class="title text-lg font-medium text-white">Hassane Barry</h3>
                    <p class="text-secondary-300 text-sm">{{ __('Cargo Head') }}</p>
                    <div
                        class="absolute -top-5 right-0 flex gap-x-1 bg-white dark:bg-secondary-600 p-4">
                        <a href="#" class="hover:-translate-y-1 transition duration-500"><x-icons.facebook class="w-8 h-8 text-secondary-500 dark:text-white" /></a>
                        <a href="#" class="hover:-translate-y-1 transition duration-500"><x-icons.linked-in class="w-8 h-8 text-secondary-500 dark:text-white" /></a>
                        <a href="#" class="hover:-translate-y-1 transition duration-500"><x-icons.instagram class="w-8 h-8 text-secondary-500 dark:text-white" /></a>
                    </div>
                </div>
            </div>
            
        </div>
        
    </div>
</section>
