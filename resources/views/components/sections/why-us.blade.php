<section class="py-16" id="why-us" aria-label="{{ __('Why us') }}">
    <div class="grid grid-cols-1 lg:grid-cols-2">
        <div class="bg-secondary-500 w-full h-96 lg:h-full p-8">
            <div class="relative">
                <img src="{{ asset('images/why-us-img.jpg') }}" class="w-full max-h-96 mt-20 lg:ml-12"
                    alt="{{ __('Why us') }}">
                <div
                    class="w-full lg:w-2/3 p-4 bg-gradient-to-r from-secondary-500 to-secondary-400 absolute bottom-0 inset-x-0 mx-auto flex items-center gap-x-4">
                    <x-icons.transport class="w-16 h-16 text-white" />
                    <h3 class="text-lg font-bold text-white">{{ __('Moving your products across world') }}</h3>
                </div>
            </div>
        </div>
        <div class="py-20 bg-gray-100 dark:bg-secondary-700 lg:pl-8">
            <div class="container space-y-6 lg:pt-8">
                <x-subtitle class="text-xs bg-secondary-600/5 dark:bg-white/5 inline-block pr-1">
                    {{ __('Why us') }}
                </x-subtitle>
                <h3 class="section-title">{{ __('We Create An Opportunity To Reach Business Potential') }}</h3>
                <p>{{ __('Space Group is a distinguished supply chain management firm which provides comprehensive solutions for warehousing, transportation and a host of logistics services.') }}
                </p>
                <div class="space-y-2 grid grid-cols-1 md:grid-cols-2">
                    <div class="space-y-1 flex items-center gap-x-2">
                        <x-icon-button class="w-12 h-12 bg-secondary-500/10 dark:bg-secondary-500/80">
                            <x-icons.box class="w-6 h-6 text-secondary-200" />
                        </x-icon-button>
                        <h3 class="title text-lg">{{ __('Safe Package') }}</h3>
                    </div>
                    <div class="space-y-1 flex items-center gap-x-2">
                        <x-icon-button class="w-12 h-12 bg-secondary-500/10 dark:bg-secondary-500/80">
                            <x-icons.global class="w-6 h-6 text-secondary-200" />
                        </x-icon-button>
                        <h3 class="title text-lg">{{ __('Global Tracking') }}</h3>
                    </div>
                    <div class="space-y-1 flex items-center gap-x-2">
                        <x-icon-button class="w-12 h-12 bg-secondary-500/10 dark:bg-secondary-500/80">
                            <x-icons.time class="w-6 h-6 text-secondary-200" />
                        </x-icon-button>
                        <h3 class="title text-lg">{{ __('In Time Delivery') }}</h3>
                    </div>
                    <div class="space-y-1 flex items-center gap-x-2">
                        <x-icon-button class="w-12 h-12 bg-secondary-500/10 dark:bg-secondary-500/80">
                            <x-icons.transport class="w-6 h-6 text-secondary-200" />
                        </x-icon-button>
                        <h3 class="title text-lg">{{ __('Ship Everywer') }}</h3>
                    </div>
                    <div class="space-y-1 flex items-center gap-x-2">
                        <x-icon-button class="w-12 h-12 bg-secondary-500/10 dark:bg-secondary-500/80">
                            <x-icons.customer-support class="w-6 h-6 text-secondary-200" />
                        </x-icon-button>
                        <h3 class="title text-lg">{{ __('24/7 Support') }}</h3>
                    </div>
                    <div class="space-y-1 flex items-center gap-x-2">
                        <x-icon-button class="w-12 h-12 bg-secondary-500/10 dark:bg-secondary-500/80">
                            <x-icons.cost class="w-6 h-6 text-secondary-200" />
                        </x-icon-button>
                        <h3 class="title text-lg">{{ __('Transparent Pricing') }}</h3>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
