<section class="py-16 bg-secondary-500" aria-label="{{ __('Contact') }}">
    <div class="container">
        <div class="grid grid-cols-1 md:grid-cols-2 gap-8">
            <div class="space-y-4">
                <x-subtitle class="text-xs bg-secondary-600/5 dark:bg-white/5 w-max pr-1">
                    <p class="text-white">{{ __('Contact') }}</p>
                </x-subtitle>
                <h2 class="section-title text-white">{{ __('Get In Touch With Us') }}</h2>
                <p class="text-gray-100">
                    {{ __('We appreciate your interest please complete the form below and we will contact you immediately to discuss your warehousing and distribution, air, ocean freight or any other logistics needs.') }}
                </p>
                <ul class="mt-6 flex flex-col gap-y-2">
                    <li class="flex items-center gap-x">
                        <x-buttons.circular-icon-button
                            class="text-secondary-200 h-14 w-14 bg-secondary-500/50 border border-secondary-400/40"
                            iconClass="w-6 h-6" name="icons.whatsapp" />
                        <a href=" https://wa.me/+224611335050"  class="text-gray-200 text-sm font-medium flex flex-col">
                            <span>WhatSapp</span>
                            <span>+224 623 654 353</span>
                        </a>
                    </li>
                    <li class="flex items-center gap-x">
                        <x-buttons.circular-icon-button
                            class="text-secondary-200 h-14 w-14 bg-secondary-500/50 border border-secondary-400/40"
                            iconClass="w-6 h-6" name="icons.envelop" />
                        <a href="#" class="text-gray-200 text-sm font-medium flex flex-col">
                            <span>Email</span>
                            <span>contact@spacegroup-gn.com</span>
                        </a>
                    </li>
                    <li class="flex items-center gap-x-2">
                        <x-buttons.circular-icon-button
                            class="text-secondary-200 h-14 w-14 bg-secondary-500/50 border border-secondary-400/40"
                            iconClass="w-6 h-6" name="icons.phone" />
                        <a href="#" class="text-gray-200 text-sm font-medium flex flex-col">
                            <span>Call Us</span>
                            <span>+224 611 335 050</span>
                        </a>
                    </li>
                </ul>
            </div>
           <livewire:form.contact-form/>
        </div>
    </div>

</section>
