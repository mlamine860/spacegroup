<section class="py-16" aria-label="{{ __('Blog') }}">
    <div class="container">
        <div class="flex flex-col lg:items-center justify-center gap-y-6">
            <x-subtitle class="text-xs bg-secondary-600/5 dark:bg-white/5 w-max pr-1">{{ __('Our Blog') }}</x-subtitle>
            <h2 class="section-title">{{ __('Our Latest News') }}</h2>
        </div>
        <livewire:blog.post-list />
        <div class="flex items-center justify-center">
            <a href="{{route('blog')}}" wire:navigate class="underline text-secondary-300">{{__('Read Our Blog')}}</a>
        </div>
    </div>
</section>
