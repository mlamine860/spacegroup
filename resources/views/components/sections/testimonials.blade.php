<section class="py-16" id="Testimonials" aria-label="{{ __('Testimonials') }}">
    <div class="container space-y-2">
        <x-subtitle class="text-xs bg-secondary-600/5 dark:bg-white/5 inline-block pr-1">
            {{ __('Testimonials') }}
        </x-subtitle>
        <h2 class="section-title">{{ __('What our customers say') }}</h2>
        <div class="relative pt-10" x-data="swiper">
            <div class="swiper" x-ref="swiper">
                <div class="swiper-wrapper">
                    @foreach ($testimonies as $testimony)
                        <div
                            class="swiper-slide p-8 {{ $loop->even ? 'bg-secondary-500' : 'bg-gray-200 dark:bg-gray-700' }}">
                            <div class="flex items-center justify-between">
                                <div class="flex items-center gap-x-4">
                                    <img src="{{ asset($testimony->avatarUrl) }}" alt="{{ $testimony->name }}"
                                        class="w-20 h-20 rounded-full">
                                    <div>
                                        <h3 class="title font-bold text-xl dark:text-white {{ $loop->even ? 'text-white' : '' }}">{{ $testimony->name }}</h3>
                                        <p class="text-secondary-400 text-sm font-medium dark:text-gray-200">
                                            {{ $testimony->job }}</p>
                                    </div>
                                </div>
                                <x-icon-button class="bg-gradient-to-r from-secondary-400 to-secondary-200 h-8 w-8">
                                    <x-icons.double-quotes class="w-6 h-6 text-white" />
                                </x-icon-button>
                            </div>
                            <div class="py-4 space-y-2">
                                <p class="text-gray-400">
                                    {{ $testimony->text }}
                                </p>
                                <div class="flex items-center gap-x-1">
                                    @for ($i = 0; $i < $testimony->stars; $i++)
                                        <x-icons.star class="w-6 h-6 fill-yellow-400 stroke-none" />
                                    @endfor

                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
            @if ($testimonies->count())
                <div
                    class="lg:absolute mt-1 lg:mt-0 lg:w-max top-0 right-0 z-40 flex items-center justify-center gap-x-2">
                    <x-icon-button tabindex="0" @click="swiper.slidePrev()"
                        class="w-7 h-7 bg-secondary-500 focus:ring-2 focus:ring-secondary-200 ring-offset-2">
                        <x-icons.arrow-left class="text-white w-5 h-5 cursor-pointer" />
                    </x-icon-button>
                    <x-icon-button tabindex="0" @click="swiper.slideNext()"
                        class="w-7 h-7 bg-secondary-500 focus:ring-2 focus:ring-secondary-200 ring-offset-2">
                        <x-icons.arrow-right class="text-white w-5 h-5 cursor-pointer" />
                    </x-icon-button>
                </div>
            @endif
        </div>
    </div>
</section>
