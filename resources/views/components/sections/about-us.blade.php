<section class="pt-16" id="about-us" aria-label="{{ __('About Us') }}">
    <div class="min-h-[50vh] lg:min-h-screen w-full bg-cover bg-no-repeat"
        style="background-image: url('{{ asset('images/team.jpg') }}')"></div>
    <div class="bg-white dark:bg-secondary-800 w-full pt-16">
        <div class="container">
            <div class="min-h-screen w-full bg-white dark:bg-secondary-800 -mt-72 p-6 lg:p-10">
                <div class="space-y-6 lg:flex lg:gap-x-8 relative">
                    <div class="space-y-6 flex-1">
                        <x-subtitle class="text-xs bg-secondary-600/5 dark:bg-white/5 inline-block pr-1">
                            {{ __('Why Us') }}
                        </x-subtitle>
                        <h2 class="section-title">{{ __('We Provide Full Range Global Logistics Solution') }}</h2>
                        <p>
                            {{ __('Space Group is a new player in the field of freight transport. A young, dynamic group focused on innovation. Let\'s move forward together.') }}
                        </p>
                        <div class="space-y-2">
                            <div class="space-y-1 flex items-center gap-x-2">
                                <x-icon-button class="w-12 h-12 bg-secondary-500/10 dark:bg-secondary-500/30">
                                    <x-icons.box class="w-6 h-6 text-secondary-200" />
                                </x-icon-button>
                                <h3 class="title text-lg">{{ __('Delivery on Time') }}</h3>
                            </div>
                            <div class="space-y-1 flex items-center gap-x-2">
                                <x-icon-button class="w-12 h-12 bg-secondary-500/10 dark:bg-secondary-500/30">
                                    <x-icons.cost class="w-6 h-6 text-secondary-200" />
                                </x-icon-button>
                                <h3 class="title text-lg">{{ __('Optimized Travel Cost') }}</h3>
                            </div>
                        </div>
                    </div>
                    <img src="{{ asset('images/about-truck.jpg') }}" alt="Spacegroup camions">
                    <div class="p-5 hidden lg:block absolute inset-x-0  mx-auto bottom-0 w-72 bg-white">
                        <img src="{{ asset('images/team-boxing.jpg') }}" class="" alt="Spacegroup team boxiong">
                    </div>
                </div>
                <div class="pt-16">
                    <div
                    class="text-center border-t border-b border-secondary-400/20 py-8 lg:py-0 space-y-6 lg:space-y-0 lg:flex lg:items-center lg:justify-center lg:gap-x-32">
                    <div
                        class="lg:flex lg:items-center lg:justify-center lg:gap-4 lg:py-8 lg:border-r lg:border-secondary-400/20 flex-1">
                        <div class="section-title text-4xl lg:text-6xl space-y-1">694</div>
                        <div class="title font-bold">{{ __('Delivered Packages') }}</div>
                    </div>
                    <div class="lg:flex lg:items-center lg:justify-center lg:gap-4 lg:py-8 flex-1">
                        <div class="section-title text-4xl lg:text-6xl space-y-1">194</div>
                        <div class="title font-bold">{{ __('Satisfied Clients') }}</div>
                    </div>
                </div>
                </div>
            </div>
        </div>
    </div>
</section>
