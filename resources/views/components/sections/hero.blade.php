<x-heading class="pt-40 pb-16 bg-secondary-700  relative overflow-hidden" aria-label="{{ __('Hero') }}">
    <video autoplay muted loop class="absolute inset-0 w-full">
        <source src="{{ asset('videos/hero-bg.mp4') }}" type="video/mp4">
    </video>
    <div class="absolute inset-0"
        style="background-image: linear-gradient(to right, rgba(9, 47, 66, 0.9), rgba(9, 47, 66, 0.6))">
    </div>
    <div class="container flex relative">
        <div class="flex flex-col items-start  gap-y-6">
            <x-subtitle class="inline-block text-lg text-white bg-secondary-500/50 w-max  pr-2 capitalize">
                Logistics & Supply Chain Solutions
            </x-subtitle>
            <h1 class="section-title text-5xl lg:text-7xl text-white">{!! __('Welcome to <br> Space Group') !!}</h1>
            <p class="section-title font-sans text-xl font-medium  text-white">
                Solutions Intégrées en Logistique, Transport, <br> Supply Chain et Performance Industrielle
            </p>
            <a href="#solutions" class="button px-6 py-4">Explorer plus</a>
        </div>
    </div>
</x-heading>
