<div
    {{$attributes->merge([
        'class' => 'rounded-full flex justify-center items-center'
    ])}}>
    {{ $slot }}
</div>
