<div {{ $attributes->merge([
    'class' => 'w-full md:w-auto  pl-2 py-1 bg-transparent border-l-4 border-primary-500 title font-medium',
]) }}>
    {{ $slot }}
</div>
