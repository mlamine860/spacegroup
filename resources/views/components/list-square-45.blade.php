<li {{ $attributes->merge([
    'class' => 'flex items-center',
]) }}>
    <span class="inline-block h-2 w-2 mr-2 rotate-45 bg-secondary-300"></span>
    {{ $slot }}
</li>
