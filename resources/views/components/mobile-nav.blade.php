<ul class="py-8">
    @foreach (config('site.main_menu') as $menu)
        @if (isset($menu['child']))
            <li class="py-2">
                <div class=" text-gray-200 font-medium hover:text-secondary-200/80 transition" x-data="{openChild: false}">
                    <button @click="openChild = !openChild" class="flex items-center">
                        <span>{{ __($menu['label']) }}</span>
                        <x-icons.chevron-down class="ml-1 w-3 h-3" />
                    </button>
                    <div x-show="openChild" x-transiton x-cloak class="py-2 max-w-[260px]" role="none">
                        @foreach ($menu['child'] as $childMenu)
                            <a href="{{url($childMenu['url']) }}" {{ !isset($childMenu['external']) ? 'wire:navigate' : 'target=blank'}}
                                class="text-gray-200 block pl-4 py-1 hover:text-secondary-200/80 text-sm transition">
                                {{ __($childMenu['label']) }}</a>
                        @endforeach
                    </div>
                </div>
            </li>
        @else
            <li class="py-2">
                <a href="{{$menu['url']}}" wire:navigate
                    class="text-gray-300 font-medium hover:text-secondary-200/80 transition">{{ __($menu['label']) }}</a>
            </li>
        @endif
    @endforeach
</ul>
