@php
    SEOMeta::setTitle(__('Server Error'))
@endphp
<x-layouts.app>
    <x-heading class="py-32 bg-no-repeat bg-cover flex items-center"
        style="background-image:
linear-gradient(to right, rgba(9, 47, 66, 1), rgba(9, 47, 66, 0)),
url('{{ asset('images/about-truck.jpg') }}')">
        <div class="container space-y-4">
            <x-subtitle class="text-white bg-secondary-500/60  pr-2 w-max">{{ __('Error') }}</x-subtitle>
            <h1 class="section-title text-white">{{ __('Error') }} 500</h1>
        </div>
    </x-heading>
    <section class="py-16">
        <div class="container">
            <div class="flex flex-col items-center justify-center gap-y-4">
                <h2 class="section-title text-9xl">5<span class="text-primary-500">00</span></h2>
                <div class="lg:max-w-screen-sm text-center">
                    <p>{{__('An internal server error has occurred. Please retry later.')}}</p>
                    <p>{{__('We apologize for any inconvenience this may cause.')}}</p>
                </div>
            </div>
    </section>
</x-layouts.app>
