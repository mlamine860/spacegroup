@php
    SEOMeta::setTitle(__('Page Not Found'))
@endphp
<x-layouts.app>
    <x-heading class="py-32 bg-no-repeat bg-cover flex items-center"
        style="background-image:
linear-gradient(to right, rgba(9, 47, 66, 1), rgba(9, 47, 66, 0)),
url('{{ asset('images/about-truck.jpg') }}')">
        <div class="container space-y-4">
            <x-subtitle class="text-white bg-secondary-500/60  pr-2 w-max">{{ __('Error') }}</x-subtitle>
            <h1 class="section-title text-white">{{ __('Page Not Found') }}</h1>
        </div>
    </x-heading>
    <section class="py-16">
        <div class="container">
            <div class="flex flex-col items-center justify-center gap-y-4">
                <h2 class="section-title text-9xl">4<span class="text-primary-500">0</span>4</h2>
                <p class="title text-2xl font-semibold">Oops! {{ __('Page Not Found') }}</p>
                <div class="lg:max-w-screen-sm text-center">
                    <p>
                        {{ __("The page you are looking for doesn't exist. Please try searching for some other page, or return to the website's homepage to find what you're looking for.") }}
                    </p>
                </div>
                <a href="{{route('home')}}" wire:navigate class="button px-6 py-4">{{ __('Back to home') }}</a>
            </div>
        </div>
    </section>
</x-layouts.app>
