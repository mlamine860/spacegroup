<div class="py-1" role="none" x-cloak>
    @foreach (config('site.locales') as $key => $local)
        <button wire:click="changeLocale('{{$key}}')"  @click.outside="openLangBox = false" class="text-gray-200 block px-4 py-1 text-sm cursor-pointer hover:text-secondary-200/80 transition" role="menuitem"
            tabindex="-1" id="menu-item-1">
            {{ __($local) }}</button>
    @endforeach
</div>
