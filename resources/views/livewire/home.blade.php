<div>
    <x-sections.hero />
    <x-sections.solutions />
    <x-sections.about-us />
    <x-sections.projects />
    <x-sections.testimonials />
    <x-sections.why-us />
    {{-- <x-sections.team /> --}}
    <x-sections.contact />
    <x-sections.blog />
</div>
