<form class="space-y-4 w-full pt-8" wire:submit="save">
    <div class="flex flex-col lg:flex-row gap-4">
        <div class="w-full">
            <x-label class="text-white" for="name" value="{{ __('Name') }}" />
            <x-form.input id="name" class="block mt-1 w-full {{ $errors->has('name') ? 'border-primary-300' : '' }}"
                type="text" wire:model="name" required autocomplete="name" />
            @error('name')
                <p class="text-sm text-primary-300 font-medium mt-1">{{ $message }}</p>
            @enderror
        </div>
        <div class="w-full">
            <x-label class="text-white" for="email" value="{{ __('Email') }}" />
            <x-form.input id="email"
                class="block mt-1 w-full {{ $errors->has('email') ? 'border-primary-300' : '' }}" type="email"
                wire:model="email" required autocomplete="email" />
            @error('email')
                <p class="text-sm text-primary-300 font-medium mt-1">{{ $message }}</p>
            @enderror
        </div>
    </div>
    <div class="flex flex-col lg:flex-row gap-4">
        <div class="w-full">
            <x-label class="text-white" for="phone" value="{{ __('Phone') }}" />
            <x-form.input id="phone"
                class="block mt-1 w-full {{ $errors->has('phone') ? 'border-primary-300' : '' }}" type="text"
                wire:model="phone" required autocomplete="phone" />
            @error('phone')
                <p class="text-sm text-primary-300 font-medium mt-1">{{ $message }}</p>
            @enderror
        </div>

        <div class="w-full">
            <x-label class="text-white" for="address" value="{{ __('Address') }}" />
            <x-form.input id="address"
                class="block mt-1 w-full {{ $errors->has('address') ? 'border-primary-300' : '' }}" type="text"
                wire:model="address" required autocomplete="address" />
            @error('address')
                <p class="text-sm text-primary-300 font-medium mt-1">{{ $message }}</p>
            @enderror
        </div>

    </div>
    <div class="flex flex-col lg:flex-row gap-4">
        <div class="w-full">
            <x-label class="text-white" for="company">
                {{ __('Company') }} <span class="text-xs font-italic opacity-50">({{ __('Optional') }})</span>
            </x-label>
            <x-form.input id="company"
                class="block mt-1 w-full {{ $errors->has('company') ? 'border-primary-300' : '' }}" type="text"
                wire:model="company"  autocomplete="company" />
            @error('company')
                <p class="text-sm text-primary-300 font-medium mt-1">{{ $message }}</p>
            @enderror
        </div>
        <div class="w-full">
            <x-label class="text-white" for="subject" value="{{ __('Subject') }}" />
            <x-form.input id="subject"
                class="block mt-1 w-full {{ $errors->has('subject') ? 'border-primary-300' : '' }}" type="text"
                wire:model="subject" required autocomplete="subject" />
            @error('subject')
                <p class="text-sm text-primary-300 font-medium mt-1">{{ $message }}</p>
            @enderror
        </div>
    </div>

    <div>
        <x-label class="text-white" for="message" value="{{ __('Message') }}" />
        <x-form.textarea rows="3" id="message"
            class="block mt-1 w-full {{ $errors->has('message') ? 'border-primary-300' : '' }}" type="text"
            wire:model="message" required autocomplete="message">
        </x-form.textarea>
        @error('message')
            <p class="text-sm text-primary-300 font-medium mt-1">{{ $message }}</p>
        @enderror
    </div>
    <x-button class="py-4 px-6" wire:loading.class="hidden" type="submit">{{ __('Submit Message') }}</x-button>
    <x-spinner class="hidden" wire:loading.class.remove="hidden" />
</form>
