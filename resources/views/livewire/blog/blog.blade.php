<div>
    @unless ($latest)
        <x-heading class="flex py-32 items-center min-h-[40vh]">
            <div class="container">
                <h1 class="italic text-2xl font-medium">
                    Aucun article disponible pour le moment
                </h1>
            </div>
        </x-heading>
    @else
        <x-heading class="py-16 lg:py-44 flex items-center bg-cover bg-no-repeat max-h-screen"
            style="background-image: linear-gradient(to right, rgba(9, 47, 66, 0.8), rgba(9, 47, 66, 0.2)), url('{{ $latest->bannerUrl }}')">
            <div class="container">
                <div class="max-w-screen-md space-y-6">
                    <h1 class="section-title text-white lg:text-4xl lg:mt-12">
                        {{ $latest->title }}
                    </h1>
                    <div class="font-medium text-sm text-slate-100">
                        <div>
                            {{ $latest->created_at->format('d M Y') }} | {{ $latest->category->name }} |
                            {{ $latest->author->name }}
                        </div>
                        <ul class="list-disc ml-4 text-sm font-medium py-2">
                            @foreach ($latest->tags as $tag)
                                <li>
                                    <a href="" class="underline">{{ $tag->name }}</a>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                    <p class="text-white">
                        {{ $latest->excerpt }}
                    </p>
                    <x-button-link href="{{ route('posts.show', $latest) }}" wire:navigate
                        class="px-6 py-4 mt-4 bg-gradient-to-r from-secondary-500 to-secondary-300 hover:from-secondary-600 hover:to-secondary-400 transition rounded">{{ __('Read More') }}</x-button-link>
                </div>
            </div>
        </x-heading>
        <section class="py-16">
            <div class="container">
                <div class="md:max-w-5xl md:m-auto ">
                    @if ($blogPosts->isNotEmpty())
                        <div class="flex flex-col lg:flex-row gap-6">
                            <x-form.input id="company" class="block  w-full" type="text" wire:model.live="search"
                                placeholder="{{ __('Search...') }}" />
                            <select wire:model.live="category"
                                class="bg-slate-100 border-secondary-200/30 focus:ring-secondary-200/40 dark:bg-secondary-700 rounded">
                                <option selected>{{ __('Category') }}</option>
                                @foreach ($categories as $categorySelect)
                                    <option value="{{ $categorySelect->id }}">{{ $categorySelect->name }}</option>
                                @endforeach
                            </select>

                        </div>
                    @endif
                    <ul class="pt-8">
                        @forelse ($blogPosts as $post)
                            <li class="py-12 space-4 lg:grid grid-cols-1 lg:grid-cols-2  lg:gap-x-2">
                                <div
                                    class="grid grid-cols-1 md:grid-cols-3 gap-1 lg:border-r border-slate-300/50 dark:border-slate-300/10 md:mr-4">
                                    <a href="{{ route('posts.show', $post) }}" class="block col-span-2" wire:navigate>
                                        <div class="relative group">
                                            <img src="{{ $post->bannerUrl }}" class="w-full h-full"
                                                alt="{{ $post->title }}">
                                            <div
                                                class="hidden absolute inset-x-0 inset-y-0 m-auto w-2/4 h-2/4 bg-secondary-400 group-hover:flex items-center justify-center transition">
                                                <span class="text-white font-medium uppercase">{{ __('Read More') }}</span>
                                            </div>
                                        </div>
                                    </a>
                                    <div class="flex items-center md:flex-col gap-2 my-2 lg:my-0">
                                        <x-icons.calendar class="w-10 h-10 text-secondary-200" />
                                        <div class="flex items-center flex-col">
                                            <span class="section-title">{{ $post->created_at->format('d') }}</span>
                                            <span>{{ $post->created_at->format('F') }}</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="space-y-4">
                                    <h3 class="title text-xl font-semibold hover:text-primary-500 transition">
                                        <a href="{{ route('posts.show', $post) }}" wire:navigate>{{ $post->title }}</a>
                                    </h3>
                                    <p class="dark:text-gray-200">
                                        {{ $post->excerpt }}
                                    </p>
                                    <ul class="list-disc ml-4 text-gray-500 text-sm font-medium">
                                        @foreach ($post->tags as $tag)
                                            <li>{{ $tag->name }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            </li>
                        @empty
                            <p>{{ __('No post found') }}</p>
                        @endforelse
                    </ul>
                </div>

            </div>
        </section>
    @endunless

</div>
