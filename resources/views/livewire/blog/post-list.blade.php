<ul class="gap-6 pt-8 md:max-w-5xl md:m-auto">
    @foreach ($posts as $post)
        <li
            class="lg:border-t border-slate-300/50 dark:border-slate-300/10 py-12 space-4 lg:grid grid-cols-1 lg:grid-cols-2  lg:gap-x-2">
            <div
                class="grid grid-cols-1 md:grid-cols-3 gap-1 lg:border-r border-slate-300/50 dark:border-slate-300/10 md:mr-4">
                <a href="{{ route('posts.show', $post) }}" class="block col-span-2" wire:navigate>
                    <div class="relative group">
                        <img src="{{ $post->bannerUrl }}" class="w-full h-full" alt="{{ $post->title }}">
                        <div
                            class="hidden absolute inset-x-0 inset-y-0 m-auto w-2/4 h-2/4 bg-secondary-400 group-hover:flex items-center justify-center transition">
                            <span class="text-white font-medium uppercase">{{ __('Read More') }}</span>
                        </div>
                    </div>
                </a>
                <div class="flex items-center md:flex-col gap-2 my-2 lg:my-0">
                    <x-icons.calendar class="w-16 h-16 text-secondary-200" />
                    <div class="flex items-center flex-col">
                        <span class="section-title">{{ $post->created_at->format('d') }}</span>
                        <span>{{ $post->created_at->format('F') }}</span>
                    </div>
                </div>
            </div>
            <div class="space-y-4">
                <h3 class="title text-xl font-semibold hover:text-primary-500 transition">
                    <a href="{{ route('posts.show', $post) }}" wire:navigate>{{ $post->title }}</a>
                </h3>
                <p class="text-gray-500">
                    {{ $post->excerpt }}
                </p>
                <ul class="list-disc ml-4 text-gray-500 text-sm font-medium">
                    @foreach ($post->tags as $tag)
                        <li>{{ $tag->name }}</li>
                    @endforeach
                </ul>
            </div>
        </li>
    @endforeach
</ul>
