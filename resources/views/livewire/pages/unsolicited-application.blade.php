<div>
    <x-heading class="py-32 bg-no-repeat bg-cover flex items-center"
        style="background-image:
linear-gradient(to right, rgba(9, 47, 66, 1), rgba(9, 47, 66, 0)),
url('{{ asset('images/about-truck.jpg') }}')">
        <div class="container">
            <div class="max-w-screen-lg space-y-4 m-auto">
                <h1 class="section-title text-white">{{ __('Unsolicited Applications') }}</h1>
            </div>
        </div>
    </x-heading>
    <section class="py-16">
        <div class="container">
            <div class="max-w-screen-lg m-auto bg-secondary-500 p-4 lg:p-8  rounded">
                <form class="space-y-6 w-full" wire:submit="save">
                    <div class="flex flex-col lg:flex-row gap-4">
                        <div class="w-full">
                            <x-label class="text-white" for="first_name" value="{{ __('First name') }}" />
                            <x-form.input id="first_name"
                                class="block mt-1 w-full {{ $errors->has('first_name') ? 'border-primary-300' : '' }}"
                                type="text" wire:model="first_name" required autocomplete="first_name" />
                            @error('first_name')
                                <p class="text-sm text-primary-300 font-medium mt-1">{{ $message }}</p>
                            @enderror
                        </div>
                        <div class="w-full">
                            <x-label class="text-white" for="last_name" value="{{ __('Last name') }}" />
                            <x-form.input id="last_name"
                                class="block mt-1 w-full {{ $errors->has('last_name') ? 'border-primary-300' : '' }}"
                                type="text" wire:model="last_name" required autocomplete="last_name" />
                            @error('last_name')
                                <p class="text-sm text-primary-300 font-medium mt-1">{{ $message }}</p>
                            @enderror
                        </div>

                    </div>
                    <div class="flex flex-col lg:flex-row gap-4">
                        <div class="w-full">
                            <x-label class="text-white" for="email" value="{{ __('Email') }}" />
                            <x-form.input id="email"
                                class="block mt-1 w-full {{ $errors->has('email') ? 'border-primary-300' : '' }}"
                                type="email" wire:model="email" required autocomplete="email" />
                            @error('email')
                                <p class="text-sm text-primary-300 font-medium mt-1">{{ $message }}</p>
                            @enderror
                        </div>
                        <div class="w-full">
                            <x-label class="text-white" for="phone_number" value="{{ __('Phone number') }}" />
                            <x-form.input id="phone_number"
                                class="block mt-1 w-full {{ $errors->has('phone_number') ? 'border-primary-300' : '' }}"
                                type="text" wire:model="phone_number" required autocomplete="phone_number" />
                            @error('phone_number')
                                <p class="text-sm text-primary-300 font-medium mt-1">{{ $message }}</p>
                            @enderror
                        </div>
                    </div>
                    <div class="flex flex-col lg:flex-row gap-4">
                        <div class="w-full">
                            <x-label class="text-white" for="address" value="{{ __('Address') }}" />
                            <x-form.input id="address"
                                class="block mt-1 w-full {{ $errors->has('address') ? 'border-primary-300' : '' }}"
                                type="text" wire:model="address" required autocomplete="address" />
                            @error('address')
                                <p class="text-sm text-primary-300 font-medium mt-1">{{ $message }}</p>
                            @enderror
                        </div>
                        <div class="w-full">
                            <x-label class="text-white" for="city" value="{{ __('City') }}" />
                            <x-form.input id="city"
                                class="block mt-1 w-full {{ $errors->has('city') ? 'border-primary-300' : '' }}"
                                type="text" wire:model="city" required autocomplete="city" />
                            @error('city')
                                <p class="text-sm text-primary-300 font-medium mt-1">{{ $message }}</p>
                            @enderror
                        </div>
                    </div>

                    <div class="w-full">
                        <x-label class="text-white" for="cover_letter" value="{{ __('Letter of motivation') }}" />
                        <x-form.textarea rows="3" id="cover_letter"
                            class="block mt-1 w-full {{ $errors->has('cover_letter') ? 'border-primary-300' : '' }}"
                            type="text" wire:model="cover_letter" required autocomplete="cover_letter">
                        </x-form.textarea>
                        @error('cover_letter')
                            <p class="text-sm text-primary-300 font-medium mt-1">{{ $message }}</p>
                        @enderror
                    </div>
                    <div class="flex flex-col lg:flex-row gap-4">
                        <div class="w-full">
                            <x-form.file id="cv" name="cv" size="{{ $this->cvDisplay }}"
                                accept="application/pdf,image/png,image/jpg" label="{{ __('Add your resume') }}"
                                class="block mt-1 w-full {{ $errors->has('cv') ? 'border-primary-300' : '' }}"
                                type="file" wire:model="cv" autocomplete="cv" />
                            <div class="flex flex-col">
                                <p class="text-white text-sm mt-1 opacity-80">
                                    {{ __('Only:pdf, png, jpg, less than 1M') }}
                                </p>
                                @error('cv')
                                    <p class="text-sm text-primary-300 font-medium mt-1">{{ $message }}</p>
                                @enderror
                            </div>
                        </div>

                        <div class="w-full">
                            <x-form.file id="additional_docs" name="additional_docs" size="{{ $this->docsDisplay }}"
                                multiple accept="application/pdf,image/png,image/jpg"
                                label="{{ __('Additional document') }}" required="{{ false }}"
                                class="block mt-1 w-full {{ $errors->has('additional_docs') ? 'border-primary-300' : '' }}"
                                type="file" wire:model="additional_docs" autocomplete="additional_docs" />
                            <div class="flex flex-col">
                                <p class="text-white text-sm mt-1 opacity-80">
                                    {{ __('Only:pdf, png, jpg, less than 1M') }}
                                </p>
                                @error('additional_docs.*')
                                    <p class="text-sm text-primary-300 font-medium mt-1">{{ $message }}</p>
                                @enderror


                            </div>
                        </div>
                    </div>

                    <x-button class="py-4 px-6" wire:loading.class="hidden"
                        type="submit">{{ __('Submit Message') }}</x-button>
                    <x-spinner class="hidden" wire:loading.class.remove="hidden" />
                </form>

            </div>

        </div>
    </section>
</div>
