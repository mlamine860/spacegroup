<div>
    <x-heading>
        <div class="border-b dark:border-slate-100/20 py-16 lg:pt-32">
            <div class="container">
                <h1 class="section-title lg:text-5xl">{{ $page->title }}</h1>
            </div>
        </div>
    </x-heading>
    <section class="py-8">
        <div class="container">
            <div class="mb-4 text-sm">
                Dernière mise à jour: {{ $page->updated_at->format('d-m-y') }}
            </div>
            <div
                class="prose prose-slate dark:prose-invert prose-headings:text-secondary-500 dark:prose-headings:text-slate-100 max-w-none">
                {!! $page->content !!}
            </div>
        </div>
    </section>
</div>
