<div>
    <x-heading class="py-32 bg-secondary-500 flex items-center">
        <div class="container">
            <div class="max-w-screen-lg m-auto">
                <h1 class="section-title text-white">{{ __('Our job offers') }}</h1>
            </div>
        </div>
    </x-heading>
    <section class="py-16">
        <div class="container">
            <div class="max-w-screen-lg m-auto">
                <p>{!! __(
                    'Currently we have no vacancies, but we are always interested in exceptional talent. If you are passionate about what we do and think your profile matches our culture, do not hesitate to send us an unsolicited application to :email. We keep all applications warm and review carefully as new opportunities arise. Thank you for your interest in SPACE GROUP',
                    [
                        'email' => '<a
                                        href="mailto:contact@spacegroup-gn.com"
                                        class="underline font-semibold">contact@spacegroup-gn.com</a>',
                    ],
                ) !!}</p>
            </div>
        </div>
    </section>
</div>
