<div>
    <x-heading class="flex items-center bg-secondary-500 py-44 min-h-[60vh]">
        <div class="container">

            <div class="max-w-screen-lg m-auto space-y-8">
                <h1 class="section-title text-white lg:text-5xl">
                    Harmonie logistique LOT 1
                </h1>
                <p class="text-lg text-white font-medium">
                    {{ __('Logistics Harmony LOT 1 is an integrated IT system (end to end) which allows you to plan, execute and monitor the overall performance of your transport and logistics operations.') }}
                </p>
            </div>
        </div>
    </x-heading>
    <section class="py-16">
        <div class="bg-secondary-600 max-w-screen-md lg:max-w-screen-lg m-auto py-16 lg:px-8">
            <div class="container">
                <div class="flex flex-col items-center space-y-4">
                    <h2 class="section-title text-xl text-white">
                        {{ __('contact our customer service for more information') }}</h2>
                    <livewire:form.contact-form />
                    <ul class="flex  gap-x-8">
                        <li class="flex items-center flex-col gap-y-2">
                            <x-buttons.circular-icon-button
                                class="text-secondary-200 h-14 w-14 bg-secondary-500/50 border border-secondary-400/40"
                                iconClass="w-6 h-6" name="icons.whatsapp" />
                            <a href=" https://wa.me/+224611335050"
                                class="text-gray-200 text-sm font-medium flex flex-col hover:text-secondary-200/80 transition">
                                WhatsApp
                            </a>
                        </li>
                        <li class="flex items-center flex-col gap-y-2">
                            <x-buttons.circular-icon-button
                                class="text-secondary-200 h-14 w-14 bg-secondary-500/50 border border-secondary-400/40"
                                iconClass="w-6 h-6" name="icons.envelop" />
                            <a href="mail:contact@spacegroup-gn.com"
                                class="text-gray-200 text-sm font-medium flex flex-col hover:text-secondary-200/80 transition">
                                Email
                            </a>
                        </li>
                        <li class="flex items-center flex-col gap-y-2">
                            <x-buttons.circular-icon-button
                                class="text-secondary-200 h-14 w-14 bg-secondary-500/50 border border-secondary-400/40"
                                iconClass="w-6 h-6" name="icons.envelop" />
                            <a href="tel:+224 611 335 050"
                                class="text-gray-200 text-sm font-medium flex flex-col hover:text-secondary-200/80 transition">
                                <span>+224 611 335 050</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </section>


</div>
