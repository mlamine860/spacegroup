<div class="w-full">
    <x-heading class="py-32 bg-no-repeat bg-cover flex items-center"
        style="background-image:
linear-gradient(to right, rgba(9, 47, 66, 1), rgba(9, 47, 66, 0)),
url('{{ asset('images/about-truck.jpg') }}')">
        <div class="container space-y-4">
            <x-subtitle class="text-white bg-secondary-500/60 w-max  pr-2">{{ __('Reach Us') }}</x-subtitle>
            <h1 class="section-title text-white">{{ __('Contact Us') }}</h1>
        </div>
    </x-heading>
    <section class="py-16">
        <div class="bg-secondary-600 rounded max-w-screen-md lg:max-w-screen-lg m-auto py-16 lg:px-8">
            <div class="container">
                <div class="flex flex-col items-center space-y-4">
                    <x-subtitle class="text-white bg-white/5 w-max pr-2">{{ __('Reach Us') }}</x-subtitle>
                    <h2 class="section-title text-2xl sm:text-3xl text-white">{{ __('Get In Touch With Us') }}</h2>
                    <div class="lg:w-2/3 m-auto">
                        <p class="text-gray-200">
                            {{ __('We appreciate your interest please complete the form below and we will contact you to discuss your warehousing, distribution, air, ocean freight or any other logistics needs.') }}
                        </p>
                    </div>
                    <ul class="flex  gap-x-8">
                        <li class="flex items-center flex-col gap-y-2">
                            <x-buttons.circular-icon-button
                                class="text-secondary-200 h-14 w-14 bg-secondary-500/50 border border-secondary-400/40"
                                iconClass="w-6 h-6" name="icons.whatsapp" />
                            <a href=" https://wa.me/+224611335050"
                                class="text-gray-200 text-sm font-medium flex flex-col hover:text-secondary-200/80 transition">
                                WhatsApp
                            </a>
                        </li>
                        <li class="flex items-center flex-col gap-y-2">
                            <x-buttons.circular-icon-button
                                class="text-secondary-200 h-14 w-14 bg-secondary-500/50 border border-secondary-400/40"
                                iconClass="w-6 h-6" name="icons.envelop" />
                            <a href="mail:contact@spacegroup-gn.com"
                                class="text-gray-200 text-sm font-medium flex flex-col hover:text-secondary-200/80 transition">
                                Email
                            </a>
                        </li>
                        <li class="flex items-center flex-col gap-y-2">
                            <x-buttons.circular-icon-button
                                class="text-secondary-200 h-14 w-14 bg-secondary-500/50 border border-secondary-400/40"
                                iconClass="w-6 h-6" name="icons.phone" />
                            <a href="tel:+224 611 335 050"
                                class="text-gray-200 text-sm font-medium flex flex-col hover:text-secondary-200/80 transition">
                                <span>+224 611 335 050</span>
                            </a>
                        </li>
                    </ul>
                    <div id="form" class="w-full">
                        <livewire:form.contact-form />
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="py-16 bg-gray-200 dark:bg-secondary-600">
        <div class="container">
            <div class="flex flex-col lg:flex-row gap-8">
                <div class="space-y-4 flex-1">
                    <x-subtitle class="bg-secondary-500/20 w-max pr-2">{{ __('FAQ') }}</x-subtitle>
                    <h2 class="section-title">{{ __('Frequently Asked Questions') }}</h2>
                    <div class="space-y-2 pt-8">
                        <div x-data="{ detailOpen: false }">
                            <div class="flex items-center gap-x-2 cursor-pointer" @click="detailOpen = !detailOpen">
                                <h3 class="title text-lg font-medium">
                                    {{ __('What warranties do I have for my shipments?') }}</h3>
                                <x-icons.chevron-up x-show="detailOpen" class="w-5 h-5" />
                                <x-icons.chevron-down x-show="!detailOpen" class="w-5 h-5" />
                            </div>
                            <div class="mt-2" x-show="detailOpen" x-transition>
                                <p class="text-gray-500 dark:text-gray-400">
                                    {{ __('Space Group operates under country-specific freight forward industry terms. We are members of Logistics and Freight Association. Space Group can also offer specific insurance cover on request.') }}
                                </p>
                            </div>
                        </div>

                        <div x-data="{ detailOpen: false }">
                            <div class="flex items-center gap-x-2 cursor-pointer" @click="detailOpen = !detailOpen">
                                <h3 class="title text-lg font-medium">
                                    {{ __('What is mandatory communication fee?') }}</h3>
                                <x-icons.chevron-up x-show="detailOpen" class="w-5 h-5 title" />
                                <x-icons.chevron-down x-show="!detailOpen" class="w-5 h-5 title" />
                            </div>
                            <div class="mt-2" x-show="detailOpen" x-transition>
                                <p class="text-gray-500 dark:text-gray-400">
                                    {{ __('Logistics operates under country-specific freight forward industry terms. We are members of Logistics and Freight Association. Logistics can also offer specific insurance cover on request.') }}
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="bg-secondary-500 rounded p-8 space-y-4">
                    <x-subtitle class="text-white">{{ __('Let’s Talk') }}</x-subtitle>
                    <p class="text-xl text-white">{{ __('You Need Any Help? Get Free Consultation') }}</p>
                    <div class="flex items-center gap-x-2">
                        <span
                            class="w-16 h-16 bg-secondary-500/50 border border-secondary-400/40 rounded-full flex justify-center items-center mr-2">
                            <x-icons.phone class="w-6 h-6 text-secondary-200" />
                        </span>
                        <a href="#" class="text-gray-200  flex flex-col text-sm">
                            <span>{{ __('Have Any Questions') }}</span>
                            <span>+224 611 335 050</span>
                        </a>
                    </div>
                    <x-button-link href="#form" class="py-4 px-6"
                        type="button">{{ __('Contact Us') }}</x-button-link>
                </div>
            </div>
        </div>
    </section>
</div>
