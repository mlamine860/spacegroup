<div>
    <x-heading class="py-44 flex items-center bg-cover bg-no-repeat min-h-screen"
        style="background-image: linear-gradient(to right, rgba(9, 47, 66, 0.8), rgba(9, 47, 66, 0.2)), url('{{ asset('images/team.jpg') }}')">
        <div class="container space-y-8">
            <h1 class="section-title text-white lg:text-5xl">
                {!! __('pages.discover-space.heading', ['brandName' => '<span class="text-primary-500">SPACE GROUP</span>']) !!}
            </h1>
            <div class="lg:max-w-lg">
                <p class="text-xl text-white font-medium">
                    {{ __('pages.discover-space.description') }}
                </p>
            </div>
        </div>
    </x-heading>
    <section class="py-16">
        <div class="container">
            <div class="max-w-screen-lg m-auto">
                <p>
                    {!! __('pages.discover-space.section.brand-description', [
                        'strong' => '<span class="font-semibold">LVMH, Groupe Rocher opération, Pierre Fabre, IVECO BUS, Groupe La
                                                                                    poste, Stef logistique et Le cabinet de consulting Ethicopex</span>',
                    ]) !!}
                </p>
                <div class="mt-8 space-y-4">
                    <h2 class="section-title">{{ __('pages.discover-space.section.title') }}</h2>
                    <ul class="space-y-6">
                        <li>
                            <h3 class="font-bold title underline"> {{ __('pages.discover-space.section.values.0.title') }}
                            </h3>
                            <p>
                                {{ __('pages.discover-space.section.values.0.text') }}
                            </p>
                        </li>
                        <li>
                            <h3 class="font-bold title underline"> {{ __('pages.discover-space.section.values.1.title') }}
                            </h3>
                            <p>
                                {{ __('pages.discover-space.section.values.1.text') }}
                            </p>
                        </li>
                        <li>
                            <h3 class="font-bold title underline"> {{ __('pages.discover-space.section.values.2.title') }}
                            </h3>
                            <p>
                                {{ __('pages.discover-space.section.values.2.text') }}
                            </p>
                        </li>
                        <li>
                            <h3 class="font-bold title underline"> {{ __('pages.discover-space.section.values.3.title') }}
                            </h3>
                            <p>
                                {{ __('pages.discover-space.section.values.3.text') }}
                            </p>
                        </li>
                    </ul>
                    <p class="font-medium">
                        {{ __('pages.discover-space.section.last-text') }}
                    </p>
                </div>
            </div>
        </div>
    </section>
</div>
