<div>
    <x-heading class="py-44 flex items-center bg-cover bg-no-repeat min-h-screen"
        style="background-image: linear-gradient(to right, rgba(9, 47, 66, 0.8), rgba(9, 47, 66, 0.2)), url('{{ asset('images/supporting-young-talents.jpg') }}')">
        <div class="container">
            <div class="max-w-screen-lg m-auto space-y-8">
                <x-subtitle class="bg-secondary-200/10 text-white inline-block pr-2">
                    {{__('pages.support.category')}}
                </x-subtitle>
                <h1 class="section-title text-white lg:text-5xl">
                    {{__('pages.support.heading')}}
                </h1>
                <x-button class="button-outline px-6 py-4 text-white">{{__('pages.support.button-text')}}</x-button>
            </div>
        </div>
    </x-heading>
    <section class="py-16">
        <div class="container">
            <div class="max-w-screen-lg m-auto">
                <p>{{__('pages.support.section.main-text')}}</p>
                <div class="mt-8">
                    <h2 class="section-title">
                        {{__('pages.support.section.title')}}<span class="text-primary-500">SPACE GROUP</span>:
                    </h2>
                    <ul class="py-2">
                        <li class="title text-lg">{{__('pages.support.section.list.0')}}</li>
                        <li class="mb-3">{{__('pages.support.section.list.1')}}</li>
                        <li class="title text-lg">{{__('pages.support.section.list.2')}}</li>
                        <li class="mb-3">{{__('pages.support.section.list.3')}}</li>
                        <li class="title text-lg">{{__('pages.support.section.list.4')}}</li>
                        <li class="mb-3">{{__('pages.support.section.list.5')}}</li>
                        <li class="title text-lg">{{__('pages.support.section.list.6')}}</li>
                        <li>{{__('pages.support.section.list.7')}}</li>
                    </ul>
                    <p class="font-medium">{{__('pages.support.section.last-line')}}</p>
                </div>
            </div>
        </div>
    </section>
    <div class="bg-secondary-600 py-32">
        <div class="container">
            <div class="flex flex-col items-center justify-between gap-y-8 lg:flex-row">
                <h2 class="section-title text-4xl lg:text-5xl text-white text-center">{{__('pages.support.banner-text')}}</h2>
                <x-button class="button-outline px-6 py-4 text-white">{{__('pages.support.button-text')}}</x-button>
            </div>
            <div class="grid grid-cols-1 lg:grid-cols-3 gap-8 mt-16">
                <img src="{{ asset('images/transport-left.jpg') }}" alt="">
                <img src="{{ asset('images/blog-2.jpg') }}" alt="">
                <img src="{{ asset('images/employee-happy.jpg') }}" alt="">
            </div>
        </div>
    </div>
</div>
