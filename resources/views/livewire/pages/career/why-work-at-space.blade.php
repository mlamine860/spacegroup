<div>
    <x-heading class="py-44 flex items-center bg-cover bg-no-repeat"
        style="background-image: linear-gradient(to right, rgba(9, 47, 66, 1), rgba(9, 47, 66, 0.8)), url('{{ asset('images/work-at-space.jpg') }}')">
        <div class="container space-y-4">
            <x-subtitle class="bg-secondary-200/10 text-white inline-block pr-2">{{ __('pages.career.category') }}</x-subtitle>
            <h1 class="section-title text-white lg:text-5xl">{{ __('pages.career.heading') }}</h1>
        </div>
    </x-heading>
    <section class="py-16">
        <div class="container">
            <div class="grid grid-cols-1 lg:grid-cols-5">
                <div class="lg:col-span-3 space-y-2">
                    <h2 class="section-title mb-2">{{__('pages.career.section-1.title')}}</h2>
                    <p>{{__('pages.career.section-1.lines.0')}}</p>
                    <p>{{__('pages.career.section-1.lines.1')}}</p>
                </div>
                <img src="{{ asset('images/team-work.jpg') }}" class="lg:col-span-2" alt="Team work">
            </div>
            <x-button-link href="/" class="button-outline px-6 py-4 my-12">{{__('pages.career.button-text')}}</x-button-link>
            <div class="grid grid-cols-1 lg:grid-cols-5">
                <div class="lg:col-span-3 space-y-2">
                    <h2 class="section-title mb-2">{{__('pages.career.section-2.title')}}</h2>
                    <p>{{__('pages.career.section-2.lines.0')}}</p>
                    <p>{{__('pages.career.section-2.lines.1')}}</p>
                    <p>{{__('pages.career.section-2.lines.2')}}</p>
                    <p>{{__('pages.career.section-2.lines.3')}}</p>
                    <p>{{__('pages.career.section-2.lines.4')}}</p>
                    <p>{{__('pages.career.section-2.lines.5')}}</p>
                </div>
                <img src="{{ asset('images/diversity-of-hand-touch.jpg') }}" class="lg:col-span-2"
                    alt="Diversity of hand touch">
            </div>
            <x-button-link href="/" class="button-outline px-6 py-4 mt-12">{{__('pages.career.button-text')}}</x-button-link>
        </div>
    </section>
</div>
