<div>
    <x-heading class="py-44 flex items-center bg-cover bg-no-repeat"
        style="background-image: linear-gradient(to right, rgba(9, 47, 66, 1), rgba(9, 47, 66, 0)), url('{{ asset('images/logistic-banner.jpg') }}')">
        <div class="container space-y-4">
            <x-subtitle
                class="bg-secondary-200/10 text-white inline-block pr-2">{{ __('pages.transport-logistics.category') }}</x-subtitle>
            <h1 class="section-title text-white lg:text-5xl">{{ __('pages.transport-logistics.heading') }}</h1>
        </div>
    </x-heading>
    <section class="py-16">
        <div class="container">
            <div class="max-w-screen-lg m-auto space-y-3">
                <p>{{ __('pages.transport-logistics.section.lines.0') }}</p>
                <p>{{ __('pages.transport-logistics.section.lines.1') }}</p>
                <p>{{ __('pages.transport-logistics.section.lines.2') }}</p>
                <p>{{ __('pages.transport-logistics.section.lines.3') }}</p>
                <div class="pt-4">
                    <p class="font-semibold">{{ __('pages.transport-logistics.section.lines.4') }}</p>
                </div>
                <p class="section-title text-2xl">{{ __('pages.transport-logistics.section.lines.5') }}</p>

                <ul class="list-disc space-y-4 pt-4">
                    <li>
                        <h3 class="font-medium underline">{{ __('pages.transport-logistics.section.list.0.title') }}
                        </h3>
                        <p>{{ __('pages.transport-logistics.section.list.0.desc') }} </p>
                    </li>
                    <li>
                        <h3 class="font-medium underline">{{ __('pages.transport-logistics.section.list.1.title') }}
                        </h3>
                        <p>{{ __('pages.transport-logistics.section.list.1.desc') }} </p>
                    </li>
                    <li>
                        <h3 class="font-medium underline">{{ __('pages.transport-logistics.section.list.2.title') }}
                        </h3>
                        <p>{{ __('pages.transport-logistics.section.list.2.desc') }} </p>
                    </li>
                    <li>
                        <h3 class="font-medium underline">{{ __('pages.transport-logistics.section.list.3.title') }}
                        </h3>
                        <p>{{ __('pages.transport-logistics.section.list.3.desc') }} </p>
                    </li>
                </ul>
                <div class="pt-6">
                    <p class="font-semibold">{{ __('pages.transport-logistics.section.last_line') }} </p>
                </div>
            </div>
        </div>
    </section>
</div>
