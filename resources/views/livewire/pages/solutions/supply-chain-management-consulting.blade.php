<div>
    <x-heading class="py-44 flex items-center bg-cover bg-no-repeat"
        style="background-image: linear-gradient(to right, rgba(9, 47, 66, 1), rgba(9, 47, 66, 0)), url('{{ asset('images/supply-chain-mangment-consulting.jpg') }}')">
        <div class="container">
            <div class="space-y-4 max-w-xl">
                <x-subtitle
                    class="bg-secondary-200/10 text-white inline-block pr-2">{{ __('pages.supply-chain-managment-consulting.category') }}</x-subtitle>
                <h1 class="section-title text-white lg:text-5xl">{!! __('pages.supply-chain-managment-consulting.heading') !!}</h1>
            </div>
        </div>
    </x-heading>
    <section class="py-16">
        <div class="container">
            <div class="max-w-screen-lg m-auto space-y-3">
                <p>{{ __('pages.supply-chain-managment-consulting.section.lines.0') }}</p>
                <ul>
                    <li class="title text-lg font-bold">
                        {{ __('pages.supply-chain-managment-consulting.section.list.0.title') }}</li>
                    <li>{{ __('pages.supply-chain-managment-consulting.section.list.0.items.0') }}</li>
                    <li>{{ __('pages.supply-chain-managment-consulting.section.list.0.items.1') }}</li>
                    <li>{{ __('pages.supply-chain-managment-consulting.section.list.0.items.2') }}</li>
                </ul>

                <ul>
                    <li class="title text-lg font-bold">
                        {{ __('pages.supply-chain-managment-consulting.section.list.1.title') }}</li>
                    <li>{{ __('pages.supply-chain-managment-consulting.section.list.1.items.0') }}</li>
                    <li>{{ __('pages.supply-chain-managment-consulting.section.list.1.items.1') }}</li>
                    <li>{{ __('pages.supply-chain-managment-consulting.section.list.1.items.2') }}</li>
                </ul>

                <ul>
                    <li class="title text-lg font-bold">
                        {{ __('pages.supply-chain-managment-consulting.section.list.2.title') }}
                    </li>
                    <li>{{ __('pages.supply-chain-managment-consulting.section.list.2.items.0') }}</li>
                    <li>{{ __('pages.supply-chain-managment-consulting.section.list.2.items.1') }}</li>
                    <li>{{ __('pages.supply-chain-managment-consulting.section.list.2.items.2') }}</li>
                </ul>

                <ul>
                    <li class="title text-lg font-bold">
                        {{ __('pages.supply-chain-managment-consulting.section.list.3.title') }}
                    </li>
                    <li>{{ __('pages.supply-chain-managment-consulting.section.list.3.items.0') }}</li>
                    <li>{{ __('pages.supply-chain-managment-consulting.section.list.3.items.1') }}</li>
                    <li>{{ __('pages.supply-chain-managment-consulting.section.list.3.items.2') }}</li>
                </ul>

                <ul>
                    <li class="title text-lg font-bold">
                        {{ __('pages.supply-chain-managment-consulting.section.list.4.title') }}
                    </li>
                    <li>{{ __('pages.supply-chain-managment-consulting.section.list.4.items.0') }}</li>
                    <li>{{ __('pages.supply-chain-managment-consulting.section.list.4.items.1') }}</li>
                    <li>{{ __('pages.supply-chain-managment-consulting.section.list.4.items.2') }}</li>
                </ul>
                <ul>
                    <li class="title text-lg font-bold">
                        {{ __('pages.supply-chain-managment-consulting.section.list.5.title') }}
                    </li>
                    <li>{{ __('pages.supply-chain-managment-consulting.section.list.5.items.0') }}</li>
                    <li>{{ __('pages.supply-chain-managment-consulting.section.list.5.items.1') }}</li>
                    <li>{{ __('pages.supply-chain-managment-consulting.section.list.5.items.2') }}</li>
                </ul>
                <p>{{ __('pages.supply-chain-managment-consulting.section.lines.1') }}</p>
            </div>
        </div>
    </section>
</div>
