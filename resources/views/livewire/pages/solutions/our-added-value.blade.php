<div>
    <x-heading class="py-44 flex items-center bg-cover bg-no-repeat"
        style="background-image: linear-gradient(to right, rgba(9, 47, 66, 1), rgba(9, 47, 66, 0)), url('{{ asset('images/our-add-value.jpg') }}')">
        <div class="container space-y-4">
            <h1 class="section-title text-white lg:text-5xl">{{ __('pages.our-add-values.heading') }}</h1>
        </div>
    </x-heading>
    <section class="py-16">
        <div class="container">
            <div class="max-w-screen-lg m-auto space-y-3">
                <p>{{ __('pages.our-add-values.section.lines.0') }}</p>
                <p>{{ __('pages.our-add-values.section.lines.1') }}</p>
                <p>{{ __('pages.our-add-values.section.lines.2') }}</p>
                <div class="py-6">
                    <h2 class="section-title text-2xl mb-2">{{ __('pages.our-add-values.section.list_title') }}</h2>
                    <ul class="list-disc space-y-2">
                        <li>
                            <h3 class="font-semibold">{{ __('pages.our-add-values.section.list.0.title') }}</h3>
                            <p>{{ __('pages.our-add-values.section.list.0.desc') }}</p>
                        </li>
                        <li>
                            <h3 class="font-semibold">{{ __('pages.our-add-values.section.list.1.title') }}</h3>
                            <p>{{ __('pages.our-add-values.section.list.1.desc') }}</p>
                        </li>
                        <li>
                            <h3 class="font-semibold">{{ __('pages.our-add-values.section.list.2.title') }}</h3>
                            <p>{{ __('pages.our-add-values.section.list.2.desc') }}</p>
                        </li>
                        <li>
                            <h3 class="font-semibold">{{ __('pages.our-add-values.section.list.3.title') }}</h3>
                            <p>{{ __('pages.our-add-values.section.list.3.desc') }}</p>
                        </li>
                        <li>
                            <h3 class="font-semibold">{{ __('pages.our-add-values.section.list.4.title') }}</h3>
                            <p>{{ __('pages.our-add-values.section.list.4.desc') }}</p>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
</div>
