import './bootstrap';

import ToastComponent from '../../vendor/usernotnull/tall-toasts/resources/js/tall-toasts'

document.addEventListener('alpine:init', () => {
    Alpine.data('swiper', () => ({
        swiper: undefined,
        init(){
            this.swiper = new Swiper(this.$refs.swiper, {
                autoplay: {
                    delay: 5000,
                  },
                  slidesPerView: 1,
                  freeMode: true,
                  breakpoints: {
                    1024: {
                      slidesPerView: 2,
                    },
                },
                keyboard: {
                    enabled: true,
                    onlyInViewport: false,
                  },
            })
        }
    }))

    Alpine.plugin(ToastComponent)

})