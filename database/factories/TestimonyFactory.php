<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;
use App\Models\Testimony;

class TestimonyFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Testimony::class;

    /**
     * Define the model's default state.
     */
    public function definition(): array
    {
        return [
            'name' => $this->faker->name(),
            'job' => $this->faker->regexify('[A-Za-z0-9]{60}'),
            'avatar' => $this->faker->regexify('[A-Za-z0-9]{255}'),
            'text' => $this->faker->text(),
            'stars' => $this->faker->numberBetween(-10000, 10000),
        ];
    }
}
