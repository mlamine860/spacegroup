<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;
use App\Models\Team;

class TeamFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Team::class;

    /**
     * Define the model's default state.
     */
    public function definition(): array
    {
        return [
            'first_name' => $this->faker->firstName(),
            'last_name' => $this->faker->lastName(),
            'email' => $this->faker->safeEmail(),
            'role' => $this->faker->regexify('[A-Za-z0-9]{255}'),
            'photo' => $this->faker->regexify('[A-Za-z0-9]{255}'),
            'handles' => '{}',
            'bio' => $this->faker->text(),
        ];
    }
}
