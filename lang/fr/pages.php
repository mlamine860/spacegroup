<?php

return [
    "A leader in national transportation" => "Un leader du transport nationale",

    'tips' => [
        'title' => 'Conseil en Supply Chain Management',
        'description-1' => "L'offre de conseil en Supply Chain et opérations de SPACE GROUP répond aux problématiques complexes et aux
        opportunités de développement et de sécurisation de vos opérations. En partant de votre stratégie globale, nous vous
        aidens a redéfinir votre Supply Chain et vos opérations afin de servir les objectifs de votre entreprise.",
        'list_description' => [
            'Définir une stratégie global Supply Chain management (End to End )',
            'Définir et mettre en place des niveaux de planification & partir d\'un ERP : PIC/S&OP; PDP et MRP',
            'Définir et optimiser votre siratégie achat',
            'Digitaliser votre chaine d\'approvisionnement',
        ]

    ],
    'performance' => [
        'title' => 'Conseil en performance industrielle',
        'description-1' => "Optimisez vos opérations industrielles avec nos solutions de conseil en performance, offrant une expertise complete de
            limplantation & la transformation vers l'industrie du futur.",
        'list_description' => [
            'Implantation & Modélisation des projets : sitesidesproduction, entrepdts logistiques, et postes de travail.',
            'Excellence opérationnelle : Conseil en organisation industrielle avec des approches telles que Le lean
            manufacturing/Six Sigma, Kaizen (Amélioration\'continue), TetabhQuality. Management (TQM) Business Process
            Reengineering (BPR), Customer Relationship Management (CRM), et Total Productive Maintenance (TPM).
            Diagnostic et audit des systémes de production pour identifier les opportunités d\'amelioration',
            'Management visuel (marquage / Panneaux)',
            'Digital Performance Management',
            'Identification des coût et des pertes de production d\'une usine eficonstruction d\'une feuille de route déployable',
        ]

    ],
    'Innovation throughout the supply chain' => 'Innovation tout au long de la supply chain',

    // Why Work at Space
    'career' => [
        'heading' => 'Pourquoi Travailler Chez Space',
        'category' => 'Carrière',
        'button-text' => 'Postuler à un emploi',
        'section-1' => [
            'title' => 'La clé de notre culture',
            'lines' => [
                'Rejoindre SPACE GROUP, c’est intégrer une entreprise où chacun peut être moteur pour construire des Supply Chains durables et agir ainsi pour un avenir meilleur.',
                "Nous mettons à disposition de nos collaborateurs les moyens d'impacter notre entreprise, nos clients, nos communautés et notre planète. Notre culture repose sur l'esprit d'entreprise, nourri par une passion pour l’innovation et 'exploration de nouveaux horizons. Travaillant de concert en équipe, nous exploitons la puissance de l'intelligence collective pour instaurer un changement durable. La réussite et le bien-être de chaque individu constituent le pivot de notre stratégie : notre engagement premier est de créer des conditions de travail sûres et équitables, propices à l'épanouissement de chacun. Nous avons pour objectif d'être une référence en matière de bien-être des employés. En offrant de nombreuses opportunités d'évolution professionnelle et de formation, nous sommes déterminés à relever de nouveaux défis chaque jour."
            ]
        ],
        'section-2' => [
            'title' => 'Diversité & Inclusion',
            'lines' => [
                "Chez SPACE GROUP, nous croyons fermement en la force du travail collectif, considérant qu'il constitue la clé de l'efficacité et de la performance, deux éléments cruciaux pour instaurer un changement durable. La diversité des origines, des expériences et des perspectives au sein d'une équipe la renforce. Nous aspirons à accueillir toutes ces différences.",
                "Notre entreprise s'engage à créer un environnement inclusif, alliant sécurité et bien-être, permettant à chaque individu de contribuer à la réussite personnelle et collective de l'entreprise. La confiance en nous-mêmes, notre ouverture aux nouvelles rencontres et notre collaboration nous conduisent à atteindre des performances exceptionnelles ensemble. C'est cette approche qui favorise l'apprentissage mutuel et la croissance collective.",
                "Ces convictions se manifestent concrètement au quotidien à travers nos actions. Nous n'acceptons aucune forme de comportement discriminatoire. Chaque candidat, membre de nos équipes, client, fournisseur ou partenaire bénéficie d'une relation et d'une expérience équitables. Nous leur assurons un environnement où chaque individu est respecté et valorisé, et nous attendons en retour le respect de ce principe.",
                "Au-delà de l'engagement envers la non-discrimination, nous nous engageons résolument à mettre en place de manière proactive des pratiques favorisant la diversité et l'inclusion en Guinée et dans tous les pays où nous exerçons nos activités. Nos actions, étroitement surveillées, visent à améliorer notre communication, nos formations et nos processus, contribuant ainsi à rendre notre environnement de travail toujours plus inclusif.",
                "Nous vous invitons également à vous joindre à ce mouvement.",
                "Ensemble, faisons croître la Supply Chain.",
            ]
        ]
    ],

    // Support young talents
    'support' => [
        'heading' => 'Étudiants et Jeunes Diplômés',
        'category' => "VOUS AVEZ DU TALENT ET DE L'AMBITION? REJOIGNEZ-NOUS!",
        'button-text' => 'Postuler à un emploi',
        'section' => [
            'main-text' => "Peu importe votre profil et votre expérience, rejoignez-nous pour contribuer à la croissance de
            SPACE
            GROUP et façonnons ensemble votre avenir. Que vous soyez intéressé par des fonctions opérationnelles
            ou
            de support, que vous préfériez travailler dans l'entreposage, le transport, l’excellence
            opérationnelle,
            les ventes, les ressources humaines, la finance, le juridique, le marketing, les méthodes et les
            processus, de nombreuses carrières et opportunités stimulantes vous attendent. Intégrez SPACE GROUP
            et
            prenez en main votre parcours professionnel en devenant un acteur actif de votre développement.",
            'title' => '4 Raisons de débuter chez ',
            'list' => [
                'Des missions stimulantes et variées',
                "Participez activement à notre expansion en favorisant la transformation et l'innovation, afin de répondre aux évolutions des exigences de nos clients, à la croissance du marché et à la distribution omnicanale.",
                "Un processus d’intégration personnalisé",
                "Nous souhaitons que vous vous sentiez pleinement intégré(e) chez SPACE GROUP dès le premier jour. À cette fin, nous marquerons votre arrivée par un processus d'intégration personnalisé, élaboré spécifiquement pour vous.",
                "Un management favorisant l’autonomie",
                "Intégré(e) au sein d'une équipe dynamique, vous aurez l'opportunité de travailler de manière autonome sur des projets d'envergure, tout en bénéficiant du contact et du soutien personnalisé d'un manager et d'un conseiller en ressources humaines.",
                "Graduate program:",
                "L'apprentissage est un processus continu, c'est pourquoi notre objectif dans les années à venir c’est d'offrir à nos collaborateurs l'accès à un graduat program en partenariat avec les universités guinéenne."

            ],
            'last-line' => 'Vous en sauriez plus dès que le projet sera en marche !'
        ],
        'banner-text' => 'Toujours là? Rejoignez-nous!'
    ],

    // Discover Space Group
    'discover-space' => [
        'heading' => 'Découvrir :brandName',
        'description' => 'Chez Space Group, nous vous offrons une gamme complète de solutions pour répondre à vos besoins en transport, logistique, et supply chain management',
        'section' => [
            'brand-description' => "SPACE GROUP est une société spécialisée dans le transport, la logistique
            et la supply chain. Fondée par des ingénieurs expérimentés
            en Global supply management , performance industrielle et IT, 
            notre société bénéficie d'une expertise solide acquise au sein de grandes entreprises internationales tels 
            que: :strong
            spécialisée en performance industrielle & excellence opérationnelle.",

            'title' => 'Les valeurs de Space group:',
            'values' => [
                [
                    "title" => "Innovation continue:",
                    "text" => "Nous mettons l'accent sur l'innovation constante pour développer des solutions technologiques de pointe qui améliorent l'efficacité
                    de la chaîne d'approvisionnement et réduisent l'impact environnemental."
                ],
                [
                    "title" => "Rigueur:",
                    "text" => "Nous nous engageons à fournir des solutions de qualité et à respecter les normes les plus strictes."
                ],
                [
                    "title" => "Performance:",
                    "text" => "La performance est le fruit de notre excellence au quotidien et de notre volonté d'entreprendre. Elle est la clé de la satisfaction de nos clients et de la pérennité de notre entreprise."
                ],
                [
                    "title" => "Excellence Opérationnelle:",
                    "text" => "Nous visons à dépasser vos attentes et à atteindre l'excellence à chaque étape de notre collaboration."
                ],
            ],
            "last-text" => "Les attitudes et comportements de nos collaborateurs sont guidés par nos valeurs, ancrées dans
            le respect des relations humaines."
        ],

    ],

    // Transport & Logistics
    'transport-logistics' => [
        'heading' => 'Transport et Logistique',
        'category' => 'Solutions',
        'section' => [
            'lines' => [
                'Nous sommes convaincus que la Supply Chain à le pouvoir de créer des impacts positifs en faveur de l’économie.',
                "Le client étant au cœur de notre stratégie, nous souhaitons lui apporter une réelle valeur ajoutée en proposant des solutions de logistique et de transports innovantes partout en Guinée et à l’international.",
                "Notre objectif c’est de livrer vos produits en parfait état, au bon moment et au bon endroit, tout en respectant les normes environnementales et de sécurité. Nous comprenons les contraintes auxquelles vous êtes confrontés et nous nous efforçons de les surmonter grâce à notre expertise et notre dévouement.",
                "Nous proposons une gamme complète de services de transport et de logistique pour répondre à vos besoins spécifiques. Grâce à notre vaste réseau et à notre expertise, nous assurons une livraison rapide, fiable et sécurisée de vos marchandises, tant sur le territoire guinéen qu'à l'international. Qu'il s'agisse de transport routier, maritime ou aérien, nous vous offrons des solutions sur mesure pour garantir que vos produits arrivent à destination en parfait état et dans les délais impartis.",
                "Nous vous mettrons à disposition des systèmes informatique de planification qui vous permettront de planifier, exécuter et suivre la performance globale des solutions proposées:",
                "HARMONIE LOGISTICS LOT1 & HARMONIE TRACKING LOT1"
            ],
            'list' => [
                [
                    'title' => 'Gestion des stocks:',
                    'desc' => 'Nous optimisons la rentabilité de votre entreprise en assurant une rotation efficace de vos stocks. Grâce à une analyse approfondie de vos besoins et à la mise en place de stratégies adaptées, nous réduisons les coûts associés aux stocks excédentaires et aux immobilisations, tout en garantissant une disponibilité optimale des produits.'
                ],
                [
                    'title' => 'Gestion des approvisionnements:',
                    'desc' => 'Nous prenons en charge la gestion efficace de vos approvisionnements, en nous assurant que vous disposez des quantités nécessaires de matières premières et de produits finis pour soutenir votre activité. Grâce à notre expertise en planification, en suivi des commandes et en gestion des fournisseurs, nous vous permettons de vous concentrer sur votre cœur de métier en toute tranquillité.'
                ],
                [
                    'title' => 'Transports:',
                    'desc' => 'Nos solutions de transport et de fret routiers assurent la qualité du service de transport. Que ce soit pour des prestations standards telles que les navettes de livraison, les transports longue distance, les expéditions à chargement partiel ou complet, les transports à température contrôlée et hautement sécurisés, ou encore des services spécifiques liés aux activités d’entreposage, notre offre couvre une vaste gamme de solutions adaptées à vos besoins.'
                ],
                [
                    'title' => 'Traçabilité:',
                    'desc' => "Notre outil HARMONIE TRACKING LOT1 utilise des technologies de pointe pour fournir une visibilité en temps réel sur les mouvements de marchandises, améliorant ainsi la traçabilité et la sécurité de votre chaîne d'approvisionnement."
                ],
            ],
            'last_line' => 'Le Groupe Space repose sur un fort esprit entrepreneurial. Tous ensemble, nous pensons comme des ingénieurs et agissons comme des entrepreneurs, pour inventer des solutions sur mesure au quotidien.'

        ],

    ],

    // Supply chain and performance industrial consulting
    'supply-chain-consulting' => [
        'heading' => 'Conseil en Supply Chain & performance :lineBreak industrielle.',
        'category' => 'Solutions',
        'section' => [

            'lines' => [
                "Nos experts vous accompagnent dans l'amélioration globale de votre performance en vous fournissant des conseils personnalisés adaptés à vos défis spécifiques.",
                "Grâce à notre savoir-faire, nous sommes capables d’effectuer une analyse approfondie de vos processus actuels afin de vous proposer des solutions sur mesure adaptées à vos besoins spécifiques. Par exemple, nous pouvons à travers un audit industriel, analyser les opérations réalisées dans votre chaine de valeur afin d’améliorer la performance du processus ou accélérer la vitesse d’exécution du flux.",
                "Grace à notre expertise en Lean management, nous pouvons élaborer et mettre en œuvre la stratégie industrielle de votre organisation, les prestations d’ingénierie et animer un écosystème industriel adapté."
            ],
            'list' => [
                "Définir une stratégie globale Supply Chain management (End to End).",
                "Définir et mettre en place des niveaux de planification à partir d’un ERP (SAP par exemple) : PIC/S&OP ; PDP et MRP.",
                "Définir et implémenter un entrepôt et une ligne de production.",
                "Industrialiser et optimiser la chaîne logistique (flux internes et externes)",
                "Construire une stratégie industrielle robuste"
            ],
            'last_lines' =>
                [
                    "Grace à ces solutions nous sommes capable de mettre en place des programmes d'amélioration continue pour vous aider à optimiser vos processus opérationnels. Grâce à des méthodes telles que le Lean Management et le Six Sigma, nous identifions les inefficacités, éliminons les gaspillages et favorisons l'innovation, vous permettant ainsi d'atteindre une performance opérationnelle supérieure.",
                    "Nous vous aidons à élaborer un plan industriel et commercial solide, adapté à vos besoins spécifiques. En optimisant votre capacité de production, en harmonisant les flux de travail et en mettant en œuvre des pratiques de gestion efficaces, nous vous aidons à maximiser l'efficacité, à réduire les coûts et à répondre de manière proactive aux demandes changeantes du marché."
                ]

        ],

    ],


    // Supply chain and performance industrial consulting
    'our-add-values' => [
        'heading' => 'Nos Valeurs Ajoutées',
        'section' => [

            'lines' => [
                "Nos experts vous accompagnent dans l'amélioration globale de votre performance en vous fournissant des conseils personnalisés adaptés à vos défis spécifiques.",
                "Grâce à notre savoir-faire, nous sommes capables d’effectuer une analyse approfondie de vos processus actuels afin de vous proposer des solutions sur mesure adaptées à vos besoins spécifiques. Par exemple, nous pouvons à travers un audit industriel, analyser les opérations réalisées dans votre chaine de valeur afin d’améliorer la performance du processus ou accélérer la vitesse d’exécution du flux.",
                "Grace à notre expertise en Lean management, nous pouvons élaborer et mettre en œuvre la stratégie industrielle de votre organisation, les prestations d’ingénierie et animer un écosystème industriel adapté."
            ],
            'list_title' => 'Pourquoi Choisir Space Group?',
            'list' => [
                [
                    'title' => 'Expertise Internationale:',
                    'desc' => 'Notre équipe possède une vaste expérience internationale dans le domaine de la Supply Chain et de la performance industrielle.'
                ],

                [
                    'title' => 'Technologie Avancée:',
                    'desc' => 'Nous utilisons des outils technologiques de pointe pour optimiser vos opérations.'
                ],
                [
                    'title' => 'Personnalisation:',
                    'desc' => 'Nous adaptons nos services à vos besoins spécifiques pour des solutions sur mesure.'
                ],
                [
                    'title' => 'Engagement envers la Durabilité:',
                    'desc' => "Nous accordons de l'importance à la durabilité environnementale dans nos opérations."
                ],
                [
                    'title' => "Transparence et Intégrité:",
                    'desc' => "Nous opérons avec intégrité et transparence à chaque étape de notre collaboration."
                ],
            ],


        ],

    ],

    'industrial-performance-consulting' => [
        'heading' => 'Conseil en performance industrielle',
        'category' => 'Solutions',
        'section' => [
            'lines' => [
                "Entrez dans l'excellence logistique avec SPACE GROUP. Au cœur de notre engagement, nous vous accompagnons tout au long de votre trajet vers une performance industrielle optimale. Propulsés par des solutions sur mesure, adaptées à vos besoins uniques, et soutenus par une équipe d'experts dédiée à la réussite de votre
                entreprise, nous redéfinissons ensemble les standards de l'efficacité opérationnelle.",
                "Découvrez comment notre savoir-faire se traduit par une analyse approfondie de vos processus, offrant des solutions sur mesure qui propulseront votre chaîne logistique vers une nouvelle dimension de performance.",
                "Ensemble, façonnons l'avenir de votre performance industrielle. Chez Space Group, notre dévouement à l'optimisation, à l'efficacité opérationnelle et à un accompagnement continu se concrétise à travers des solutions sur mesure adaptées à vos besoins spécifiques. Avec notre
                équipe d'experts dédiée, nous sommes prêts à guider votre entreprise vers l'excellence opérationnelle, redéfinissant ainsi les normes de succès dans le domaine industriel",
            ],
            'list' => [
                [
                    'title' => "Diagnostic et audit des systèmes de production :",
                    'items' => [
                        "Analyse approfondie des processus de production.",
                        "Évaluation des équipements et des technologies utilisés.",
                        "Identification des points forts et des opportunités d'amélioration."
                    ]
                ],
                [
                    'title' => "Optimisation des Processus :",
                    'items' => [
                        "Mise en place de méthodologies Lean et Six Sigma.",
                        "Réduction des gaspillages et des coûts opérationnels.",
                        "Amélioration de l'efficacité et de la productivité."
                    ]
                ],
                [
                    'title' => "Excellence opérationnelle",
                    'subtitle' => 'Bénéficiez de conseils en organisation industrielle avec des méthodologies telles que',
                    'items' => [
                        "Lean Manufacturing/Six Sigma, Kaizen (Amélioration continue),",
                        "Total Quality Management (TQM),",
                        "Business Process Reengineering (BPR),",
                        "Customer Relationship Management (CRM),",
                        "Total Productive Maintenance (TPM).",
                    ]
                ],
                [
                    'title' => "Implantation & modélisation des projets industriels et logistiques:",
                    'items' => [
                        "Création de plans d'implantation pour les usines, entrepôts, centres de distribution, etc.",
                        "Optimisation de la disposition des équipements, des zones de stockage et des aires de travail.",
                        "Définition de flux logistiques efficaces pour minimiser les déplacements inutiles."
                    ]
                ],
                [
                    'title' => " Mise en Place de Solutions Technologiques :",
                    'items' => [
                        "Intégration de systèmes informatiques avancés.",
                        "Utilisation de l'automatisation et de l'IoT pour améliorer la gestion.",
                        "Formation du personnel aux nouvelles technologies."
                    ]
                ],
                [
                    'title' => "Formation et Développement des Compétences :",
                    'items' => [
                        "Programmes de formation personnalisés pour le personnel.",
                        "Développement des compétences techniques et managériales.",
                        "Accompagnement du changement."
                    ]
                ],
            ]
        ]
    ],
    'supply-chain-managment-consulting' => [
        'heading' => 'Conseil en Supply Chain Management',
        'category' => 'Solutions',
        'section' => [
            'lines' => [
                "SPACE GROUP, votre allié dédié à l'excellence en gestion de la chaîne logistique. Nous vous proposons des services de conseil en Supply Chain Management,
                conjuguant expertise, dévouement et une vision orientée vers l'avenir. Chez SPACE GROUP, nous comprenons l'importance de la durabilité et de la
                digitalisation dans le monde moderne des affaires. Nos solutions sur mesure intègrent des pratiques éco-responsables pour une chaîne logistique durable et
                profitent des dernières avancées technologiques pour une digitalisation efficace de vos opérations. Découvrez comment nous redéfinissons les normes de
                performance en unissant excellence opérationnelle, durabilité écologique et transformation digitale au service de votre réussite.",
                "Ensemble, forgeons le chemin vers une Supply Chain d'excellence et durable.

                Chez Space Group, notre engagement envers l'optimisation, l'efficacité opérationnelle et un accompagnement continu se traduit par des solutions sur mesure,
                taillées pour répondre spécifiquement à vos besoins. Avec notre équipe d'experts dédiée, nous façonnons le futur de votre entreprise vers une réussite logistique
                inégalée"
            ],
            'list' => [
                [
                    'title' => "Analyse et Optimisation de la Supply Chain",
                    'items' => [
                        "Audit approfondi des processus logistiques existants.",
                        "Identification des inefficacités et des opportunités d'optimisation.",
                        "Mise en place de solutions pour améliorer la fluidité des opérations."
                    ]
                ],
                [
                    'title' => "Stratégie Globale Supply Chain Management:",
                    'items' => [
                        "Définition d'une vision holistique de la gestion de la chaîne logistique.",
                        "Élaboration de plans stratégiques pour optimiser les opérations de bout en bout.",
                        "Intégration de technologies pour une visibilité accrue et une meilleure coordination."
                    ]
                ],
                [
                    'title' => "Planification Niveau ERP :",
                    'items' => [
                        "Mise en place de niveaux de planification tels que PIC/S&OP, PDP et MRP.",
                        "Utilisation d'outils ERP (comme SAP) pour une planification plus précise.",
                        "Optimisation des flux de travail pour une meilleure gestion des ressources."
                    ]
                ],
                [
                    'title' => "Conception et Mise en Place d'Entrepôts et de Lignes de Production :",
                    'items' => [
                        "Analyse des besoins en stockage et conception d'entrepôts efficaces.",
                        "Optimisation des lignes de production pour une meilleure efficacité.",
                        "Intégration de technologies de pointe pour automatiser les processus.",
                    ]
                ],
                [
                    'title' => "Industrialisation et Optimisation de la Chaîne Logistique :",
                    'items' => [
                        "Analyse des flux internes et externes pour une meilleure performance.",
                        "Mise en œuvre de solutions visant à réduire les coûts logistiques.",
                        "Utilisation de technologies de suivi et de gestion des stocks."
                    ]
                ],
                [
                    'title' => "Formation et Accompagnement du Personnel:",
                    'items' => [
                        "Formation du personnel aux nouvelles méthodologies et technologies.",
                        "Accompagnement du changement pour assurer une adoption fluide des nouvelles pratiques.",
                        "Renforcement des compétences nécessaires pour maintenir l'efficacité opérationnelle."
                    ]
                ],
               
            ]
        ]
    ]

];