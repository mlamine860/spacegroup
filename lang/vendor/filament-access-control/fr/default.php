<?php

return [
    'fields' => [
        'first_name' => 'Prénm',
        'last_name' => 'Nom',
        'email' => 'E-mail',
        'role' => 'Role',
        'permissions' => 'Permissions',
        'name' => 'Nom',
        'full_name' => 'Nom complet',
        'description' => 'Description',
        'id' => 'ID',
        'created_at' => 'Créé le',
        'password' => 'Mot de passe',
        'password_confirm' => 'Confirmez le mot de passe',
        'active' => 'Activé',
        'expires_at' => 'Date d\'expiration',
        'code' => 'Verification code',
    ],
    'resources' => [
        'admin_user' => 'Admin User',
        'admin_users' => 'Admin Users',
        'role' => 'Role',
        'roles' => 'Roles',
        'permission' => 'Permission',
        'permissions' => 'Permissions',
        'group' => 'Administration',
    ],
    'sections' => [
        'permissions' => 'Permissions',
        'user_details' => 'Détails de l\'utilisateur',
    ],
    'messages' => [
        'permissions_create' => "Les utilisateurs peuvent disposer d'autres autorisations liées à leur rôle. Les autorisations réelles sont répertoriées sur la page d'affichage.",
        'permissions_view' => 'Autorisations directes ainsi que autorisations via leur rôle.',
        'account_expired' => 'Ce compte est expiré. Veuillez contacter un administrateur.',
        'accounts_extended' => 'Les comptes sélectionnés ont été étendus.',
        'invalid_user' => 'Utilisateur invalide, veuillez réessayer.',
        'code_expired' => 'Ce code de vérification a expiré. Merci d\'utiliser le nouveau code que nous venons de vous envoyer.',
        'invalid_code' => 'Code de vérification invalide.',
        'enter_code' => 'Pour confirmer votre connexion, veuillez saisir le code de vérification envoyé à votre adresse e-mail.',
    ],
    'pages' => [
        'reset_password' => 'Réinitialiser le mot de passe',
        'account_expired' => 'Compte expiré',
        'two_factor' => 'identification',
    ],
    'notifications' => [
        'salutation' => 'Salutations',
        'password_reset' => [
            'title' => 'Votre mot de passe pour le :host administration',
            'message' => 'Vous recevez cet e-mail car nous avons reçu une demande de réinitialisation du mot de passe pour votre compte.',
            'button' => 'réinitialiser le mot de passe',
            'expiry' => 'Ce lien de réinitialisation de mot de passe expirera dans :count minutes. Si vous n\'avez pas demandé de réinitialisation du mot de passe, aucune autre action n\'est requise.',
        ],
        'password_set' => [
            'title' => 'Votre compte pour le :host administration',
            'message' => 'Vous recevez cet e-mail car un compte administrateur a été récemment créé pour vous pour l\'administrateur :host. Veuillez cliquer sur le lien suivant pour définir votre mot de passe:',
            'button' => 'Définir le mot de passe',
            'expiry' => 'Ce lien de définition de mot de passe expirera dans :count minutes. Si le lien a expiré, vous pouvez tenter de [reset your password manually](:url).',
        ],
        'two_factor' => [
            'title' => 'Votre code de vérification pour le :host administration',
            'message' => 'Pour confirmer votre connexion, veuillez utiliser le code de vérification suivant. Le code est valable 5 minutes.',
        ],
    ],
    'buttons' => [
        'back_to_login' => 'Retour connexion',
        'forgot_password' => 'Forgot password?',
        'submit' => 'Soumettre',
    ],
    'filters' => [
        'expired' => 'Expiré',
    ],
    'actions' => [
        'extend' => 'Prolonger la date d\'expiration',
    ],
];
