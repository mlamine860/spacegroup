<?php

return [
    /**
     * Dashboard Page
     */
    'title' => 'Google analytics',
    'navigation_label' => 'Analytics',

    /**
     * Widget Heading/Labels
     */
    'page_views' =>  'Pages vues',
    'visitors' => 'Utilisateurs uniques',
    'sessions' => 'Sessions',
    'sessions_duration' => 'Durée moyenne des sessions',
    'sessions_by_country' => 'Sessions par pays',
    'sessions_by_device' => 'Sessions par appareil',
    'top_referrers' => 'Meilleurs référents',
    'most_visited_pages' => 'Les pages les plus visitées',
    'one_day_active_users' => 'Utilisateurs actifs depuis 1 jour',
    'seven_day_active_users' => 'Utilisateurs actifs pendant 7 jours',
    'fourteen_day_active_users' => 'Utilisateurs actifs pendant 14 jours',
    'twenty_eight_day_active_users' => 'Utilisateurs actifs pendant 28 jours',

    /**
     * Filter Labels
     */

    'T' => 'Aujourd\'hui',
    'Y' => 'Hier',
    'LW' => 'La semaine dérnière',
    'LM' => 'Le moi dernier',
    'LSD' => 'Les 7 derniers jours',
    'LTD' => 'Les 30 derniers jours',
    'TW' => 'Cette semaine',
    'TM' => 'Ce mois-ci',
    'TY' => 'Cette année',
    'FD' => '5 jours',
    'TD' => '10 jours',
    'FFD' => '15 jours',

    /**
     * Trajectory Label Descriptions
     */
    'trending_up' => 'Augmenter',
    'trending_down' => 'Diminuer',
    'steady' => 'Constant',
];
