<?php

return [

    "A leader in national transportation" => "Un leader du transport nationale",

    'Innovation throughout the supply chain' => 'Innovation throughout the supply chain',
    // Carrère
    'career' => [
        'heading' => 'Why Work at Space',
        'category' => 'Career',
        'button-text' => 'Apply for a job',
        'section-1' => [
            'title' => 'The key to our culture',
            'lines' => [
                'Joining SPACE GROUP means joining a company where everyone can be a driving force in building sustainable Supply Chains and thus acting for a better future.',
                "We provide our employees with the means to impact our company, our customers, our communities and our planet. Our culture is based on an entrepreneurial spirit, fueled by a passion for innovation and exploring new horizons. Working together as a team, we harness the power of collective intelligence to create lasting change. The success and well-being of each individual constitute the pivot of our strategy: our primary commitment is to create safe and fair working conditions, conducive to the development of everyone. We aim to be a benchmark in employee well-being. By offering numerous opportunities for professional development and training, we are determined to take on new challenges every day."
            ]
        ],
        'section-2' => [
            'title' => 'Diversity & Inclusion',
            'lines' => [
                "At SPACE GROUP, we firmly believe in the power of collective work, considering that it constitutes the key to efficiency and performance, two crucial elements for establishing lasting change. The diversity of backgrounds, experiences and perspectives within a team strengthens it. We aspire to welcome all these differences.",
                "Our company is committed to creating an inclusive environment, combining safety and well-being, allowing each individual to contribute to the personal and collective success of the company. Confidence in ourselves, our openness to new encounters and our collaboration lead us to achieve exceptional performances together. It is this approach that promotes mutual learning and collective growth.",
                "These convictions are manifested concretely on a daily basis through our actions. We do not accept any form of discriminatory behavior. Each candidate, member of our teams, client, supplier or partner benefits from a fair relationship and experience. We provide them with an environment where each individual is respected and valued, and we expect respect for this principle in return.",
                "Beyond the commitment to non-discrimination, we are resolutely committed to proactively implementing practices that promote diversity and inclusion in Guinea and in all the countries where we operate. Our actions, closely monitored, aim to improve our communication, our training and our processes, thus contributing to making our work environment ever more inclusive.",
                "We also invite you to join this movement.",
                "Together, let’s grow the Supply Chain."
            ]
        ]
    ],

    // Support young talents
    'support' => [
        'heading' => 'Students and Young Graduates',
        'category' => "DO YOU HAVE TALENT AND AMBITION? JOIN US!",
        'button-text' => 'Apply for a job',
        'section' => [
            'main-text' => "Regardless of your profile and experience, join us to contribute to the
            growth of SPACE GROUP and let’s shape your future together. Whether you are interested in
           operational or support roles, whether you prefer to work in warehousing,
            transport, operational excellence,
           sales, human resources, finance, legal, marketing, methods and
           process, many challenging careers and opportunities await you. Join SPACE GROUP and
           take charge of your professional journey by becoming an active player in your development.",
            'title' => '4 Reasons to start at ',
            'list' => [
                'Stimulating and varied missions',
                "Actively participate in our expansion by driving transformation and innovation, in order to respond to changing customer requirements, market growth and omnichannel distribution.",
                "A personalized onboarding process",
                "We want you to feel fully integrated into SPACE GROUP from day one. To this end, we will mark your arrival with a personalized integration process, developed specifically for you.",
                "Management promoting autonomy",
                "Integrated into a dynamic team, you will have the opportunity to work independently on large-scale projects, while benefiting from the contact and personalized support of a manager and a human resources advisor. .",
                "Graduate program:",
                "Learning is a continuous process, which is why our objective in the years to come is to offer our employees access to a graduate program in partnership with Guinean universities."

            ],
            'last-line' => 'You will know more as soon as the project is underway!'
        ],
        'banner-text' => 'Still there? Join us!'
    ],

    // Discover Space Group
    'discover-space' => [
        'heading' => 'Discover :brandName',
        'description' => 'At Space Group, we offer you a complete range of solutions to meet your transport, logistics and supply chain management needs.',
        'section' => [
            'brand-description' => "SPACE GROUP is a company specializing in transport, logistics
            and the supply chain. Founded by experienced engineers
            in Global supply management, industrial performance and IT,
            our company benefits from solid expertise acquired within large international companies such as: :strong
            specialized in industrial performance & operational excellence.",

            'title' => 'Space group values:',
            'values' => [
                [
                    "title" => "Continuous innovation:",
                    "text" => "We emphasize constant innovation to develop cutting-edge technology solutions that improve efficiency
                    supply chain and reduce environmental impact."
                ],
                [
                    "title" => "Rigor:",
                    "text" => "We are committed to providing quality solutions and meeting the highest standards."
                ],
                [
                    "title" => "Performance:",
                    "text" => "Performance is the fruit of our daily excellence and our desire to undertake. It is the key to the satisfaction of our customers and the sustainability of our company."
                ],
                [
                    "title" => "Operational effectiveness:",
                    "text" => "We aim to exceed your expectations and achieve excellence at every stage of our collaboration."
                ],
            ],
            "last-text" => "The attitudes and behaviors of our employees are guided by our values, anchored in
            respect for human relations."
        ],

    ],

    // Transport & Logistics
    'transport-logistics' => [
        'heading' => 'Transport and Logistics',
        'category' => 'Solutions',
        'section' => [
            'lines' => [
                'We are convinced that the Supply Chain has the power to create positive impacts for the economy.',
                "The customer being at the heart of our strategy, we wish to provide them with real added value by offering innovative logistics and transport solutions throughout Guinea and internationally.",
                "Our goal is to deliver your products in perfect condition, at the right time and to the right place, while respecting environmental and safety standards. We understand the constraints you face and we strive to overcome them with our expertise and dedication.",
                "We offer a full range of transportation and logistics services to meet your specific needs. Thanks to our extensive network and expertise, we ensure fast, reliable and secure delivery of your goods, both in Guinea and internationally. Whether road, sea or air transport, we offer tailor-made solutions to ensure that your products arrive at their destination in perfect condition and on time.",
                "We will provide you with planning IT systems that will allow you to plan, execute and monitor the overall performance of the solutions offered:",
                "HARMONIE LOGISTICS LOT1 & HARMONIE TRACKING LOT1"
            ],
            'list' => [
                [
                    'title' => 'Inventory management:',
                    'desc' => 'We optimize the profitability of your business by ensuring efficient rotation of your stocks. Through an in-depth analysis of your needs and the implementation of appropriate strategies, we reduce costs associated with excess inventory and downtime, while ensuring optimal product availability.'
                ],
                [
                    'title' => 'Supply Management:',
                    'desc' => 'We support the efficient management of your supplies, ensuring you have the necessary quantities of raw materials and finished products to support your business. Thanks to our expertise in planning, order tracking and supplier management, we allow you to concentrate on your core business with complete peace of mind.'
                ],
                [
                    'title' => 'Transportation:',
                    'desc' => 'Our road transport and freight solutions ensure quality transport service. Whether for standard services such as delivery shuttles, long-distance transport, partial or full load shipments, temperature-controlled and highly secure transport, or even specific services linked to warehousing activities, our offer covers a wide range of solutions tailored to your needs.'
                ],
                [
                    'title' => 'Traceability:',
                    'desc' => "Our HARMONIE TRACKING LOT1 tool uses cutting-edge technologies to provide real-time visibility into goods movements, improving the traceability and security of your supply chain."
                ],
            ],
            'last_line' => 'The Space Group is based on a strong entrepreneurial spirit. All together, we think like engineers and act like entrepreneurs, to invent tailor-made solutions on a daily basis.'

        ],

    ],

    // Supply chain and performance industrial consulting
    'supply-chain-consulting' => [
        'heading' => 'Supply Chain & performance consulting :lineBreak industrial.',
        'category' => 'Solutions',
        'section' => [

            'lines' => [
                "Our experts support you in improving your overall performance by providing you with personalized advice adapted to your specific challenges.",
                "Thanks to our know-how, we are able to carry out an in-depth analysis of your current processes in order to offer you tailor-made solutions adapted to your specific needs. For example, we can, through an industrial audit, analyze the operations carried out in your value chain in order to improve process performance or accelerate flow execution speed.",
                "Thanks to our expertise in Lean management, we can develop and implement your organization's industrial strategy, engineering services and manage a suitable industrial ecosystem."
            ],
            'list' => [
                "Define a global Supply Chain management strategy (End to End).",
                "Define and implement planning levels from an ERP (SAP for example): PIC/S&OP; PDP and MRP.",
                "Define and implement a warehouse and a production line.",
                "Industrialize and optimize the supply chain (internal and external flows)",
                "Building a robust industrial strategy"
            ],
            'last_lines' =>
                [
                    "Thanks to these solutions we are able to implement continuous improvement programs to help you optimize your operational processes. Using methods such as Lean Management and Six Sigma, we identify inefficiencies, eliminate waste and drive innovation, enabling you to achieve superior operational performance.",
                    "We help you develop a solid industrial and commercial plan, tailored to your specific needs. By optimizing your production capacity, harmonizing workflows and implementing effective management practices, we help you maximize efficiency, reduce costs and proactively respond to changing market demands."
                ]

        ],

    ],

    // Supply chain and performance industrial consulting
    'our-add-values' => [
        'heading' => 'Our Added Values',
        'section' => [

            'lines' => [
                "Our experts support you in improving your overall performance by providing you with personalized advice adapted to your specific challenges.",
                "Thanks to our know-how, we are able to carry out an in-depth analysis of your current processes in order to offer you tailor-made solutions adapted to your specific needs. For example, we can, through an industrial audit, analyze the operations carried out in your value chain in order to improve process performance or accelerate flow execution speed.",
                "Thanks to our expertise in Lean management, we can develop and implement your organization's industrial strategy, engineering services and manage a suitable industrial ecosystem."
            ],
            'list_title' => 'Why Choose Space Group?',
            'list' => [
                [
                    'title' => 'International Expertise:',
                    'desc' => 'Our team has extensive international experience in the field of Supply Chain and industrial performance.'
                ],

                [
                    'title' => 'Advanced Technology:',
                    'desc' => 'We use cutting-edge technological tools to optimize your operations.'
                ],
                [
                    'title' => 'Customization:',
                    'desc' => 'We adapt our services to your specific needs for tailor-made solutions.'
                ],
                [
                    'title' => 'Commitment to Sustainability:',
                    'desc' => "We value environmental sustainability in our operations."
                ],
                [
                    'title' => "Transparency and Integrity:",
                    'desc' => "We operate with integrity and transparency at every stage of our collaboration."
                ],
            ],


        ],

    ],
    'industrial-performance-consulting' => [
        'heading' => 'Industrial performance consulting',
        'category' => 'Solutions',
        'section' => [
            'lines' => [
                "Enter the world of logistics excellence with SPACE GROUP. At the heart of our commitment, we accompany you on your journey towards optimum industrial performance. Driven by customized solutions tailored to your unique needs, and supported by a team of experts dedicated to your company's success, together we redefine the standards of operational efficiency.
                together we redefine the standards of operational efficiency.",
                "Find out how our know-how translates into in-depth analysis of your processes, offering tailor-made solutions that will propel your supply chain to a new dimension of performance.",
                "Shaping the future of your industrial performance. At Space Group, our dedication to optimization, operational efficiency and ongoing support is reflected in solutions tailored to your specific needs. With our
                dedicated team of experts, we are ready to guide your company towards operational excellence, redefining the standards for success in the industrial field.",
            ],
            'list' => [
                [
                    'title' => "Diagnosis and audit of production systems:",
                    'items' => [
                        "In-depth analysis of production processes.",
                        "Evaluation of equipment and technologies used.",
                        "Identify strengths and opportunities for improvement."
                    ]
                ],
                [
                    'title' => "Process optimization:",
                    'items' => [
                        "Implementation of Lean and Six Sigma methodologies.",
                        "Reduce waste and operational costs.",
                        "Improved efficiency and productivity."
                    ]
                ],
                [
                    'title' => "Operational excellence",
                    'subtitle' => 'Benefit from industrial organization consulting services with methodologies such as',
                    'items' => [
                        "Lean Manufacturing/Six Sigma, Kaizen (Amélioration continue),",
                        "Total Quality Management (TQM),",
                        "Business Process Reengineering (BPR),",
                        "Customer Relationship Management (CRM),",
                        "Total Productive Maintenance (TPM).",
                    ]
                ],
                [
                    'title' => "Layout and modeling of industrial and logistics projects:",
                    'items' => [
                        "Creation of layout plans for factories, warehouses, distribution centers, etc.",
                        "Optimized layout of equipment, storage and work areas.",
                        "Define efficient logistics flows to minimize unnecessary travel."
                    ]
                ],
                [
                    'title' => "Implementing Technological Solutions :",
                    'items' => [
                        "Integration of advanced IT systems.",
                        "Using automation and IoT to improve management.",
                        "Staff training in new technologies."
                    ]
                ],
                [
                    'title' => "Training and Skills Development:",
                    'items' => [
                        "Customized training programs for staff.",
                        "Development of technical and managerial skills.",
                        "Change management."
                    ]
                ],
            ]
        ]
    ],
    'supply-chain-managment-consulting' => [
        'heading' => 'Supply Chain Management Consulting',
        'category' => 'Solutions',
        'section' => [
            'lines' => [
                "SPACE GROUP, your ally dedicated to excellence in supply chain management. We offer Supply Chain Management consulting services, combining expertise, dedication and a forward-looking vision. At SPACE GROUP, we understand the importance of sustainability and digitalization in the modern business world. Our tailor-made solutions integrate eco-responsible practices for a sustainable supply chain, and take advantage of the latest technological advances to effectively digitalize your operations. Find out how we redefine performance standards by uniting operational excellence, environmental sustainability and digital transformation for your success.",
                "Together, let's forge the path to a sustainable Supply Chain of excellence. At Space Group, our commitment to optimization, operational efficiency and ongoing support translates into customized solutions tailored specifically to your needs. Together with our dedicated team of experts, we shape your company's future towards unrivalled logistics success."
            ],
            'list' => [
                [
                    'title' => "Supply Chain Analysis and Optimization",
                    'items' => [
                        "In-depth audit of existing logistics processes.",
                        "Identification of inefficiencies and optimization opportunities.",
                        "Implementation of solutions to improve the fluidity of operations."
                    ]
                ],
                [
                    'title' => "Global Strategy Supply Chain Management:",
                    'items' => [
                        "Defining a holistic vision of supply chain management.",
                        "Development of strategic plans to optimize end-to-end operations.",
                        "Integration of technologies for greater visibility and better coordination."
                    ]
                ],
                [
                    'title' => "Planning ERP level:",
                    'items' => [
                        "Implementation of planning levels such as PIC/S&OP, PDP and MRP.",
                        "Use of ERP tools (such as SAP) for more precise planning.",
                        "Optimized workflows for better resource management."
                    ]
                ],
                [
                    'title' => "Design and implementation of warehouses and production lines:",
                    'items' => [
                        "Analysis of storage requirements and design of efficient warehouses.",
                        "Optimization of production lines for greater efficiency.",
                        "Integration of cutting-edge technologies to automate processes.",
                    ]
                ],
                [
                    'title' => "Industrialization and Optimization of the Supply Chain :",
                    'items' => [
                        "Analysis of internal and external flows for improved performance.",
                        "Implement solutions to reduce logistics costs.",
                        "Use of inventory tracking and management technologies."
                    ]
                ],
                [
                    'title' => "Staff training and support:",
                    'items' => [
                        "Staff training in new methodologies and technologies.",
                        "Change management to ensure smooth adoption of new practices.",
                        "Reinforcing the skills needed to maintain operational efficiency."
                    ]
                ],
               
            ]
        ]
    ]



];