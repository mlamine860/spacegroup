<?php

use App\Livewire\Home;
use App\Livewire\Blog\Blog;
use App\Livewire\Blog\Post;
use App\Livewire\Pages\Offers;
use App\Livewire\Pages\Contact;
use App\Livewire\Pages\PrivacyPolicy;
use Illuminate\Support\Facades\Route;
use App\Livewire\Pages\About\DiscoverSpace;
use App\Livewire\Pages\Career\WhyWorkAtSpace;
use App\Livewire\Pages\Tools\HarmonyLogistic;
use App\Livewire\Pages\Tools\HarmonyTracking;
use App\Livewire\Pages\UnsolicitedApplication;
use App\Livewire\Pages\Solutions\OurAddedValue;
use App\Livewire\Pages\Solutions\TransportLogistic;
use App\Livewire\Pages\Career\SupportingYoungTalent;
use App\Livewire\Pages\Solutions\IndustrialPerformanceConsulting;
use App\Livewire\Pages\Solutions\SupplyChainManagementConsulting;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::middleware(['locale'])->group(function () {
    Route::get('/', Home::class)->name('home');

    // Solutions
    Route::prefix('solutions')->group(function () {
        Route::get('transport-logistics', TransportLogistic::class)->name('pages.transport-logistics');
        Route::get('supply-chain-managment-consulting', SupplyChainManagementConsulting::class)->name('pages.supply-chain-managment-consulting');
        Route::get('industrial-performance-consulting', IndustrialPerformanceConsulting::class)->name('pages.industrial-performance-consulting');
        Route::get('our-added-values', OurAddedValue::class)->name('pages.our-added-values');
    });

    // Tools
    Route::prefix('tools')->group(function () {
        Route::get('harmony-logistic', HarmonyLogistic::class)->name('tools.harmony-logistic');
        Route::get('harmony-tracking', HarmonyTracking::class)->name('tools.harmony-tracking');
    });

    //Pages 
    Route::get('offers', Offers::class)->name('pages.offers');
    Route::get('unsolicited-application', UnsolicitedApplication::class)->name('pages.unsolicited-application');
    Route::get('contact', Contact::class)->name('pages.contact');
    Route::get('about', DiscoverSpace::class)->name('pages.about');

    //Career
    Route::get('/why-work-at-space', WhyWorkAtSpace::class)->name('why-work-at-space');
    Route::get('/supporting-young-talents', SupportingYoungTalent::class)->name('supporting-young-talents');
    Route::get('/discover-space', DiscoverSpace::class)->name('discover-space');

    // Blog
    Route::get('blog', Blog::class)->name('blog');
    Route::get('posts/{post:slug}', Post::class)->name('posts.show');

    // Privacy Policy
    Route::get('privacy-policy', PrivacyPolicy::class);

});






